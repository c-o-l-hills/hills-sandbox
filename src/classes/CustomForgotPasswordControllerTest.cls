/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class CustomForgotPasswordControllerTest {
@IsTest(SeeAllData=true)
    public static void testCustomForgotPasswordController() {
        CustomForgotPasswordController controller = new CustomForgotPasswordController();
        
        Test.startTest();
        //controller.email = 'shiori_ohara@hillspet.com';
        controller.forgotPassword();
        Test.stopTest();
    }
}