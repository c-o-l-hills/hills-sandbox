public with sharing class ShippingSheetDetailBk {
    public map<Id, HospitalClass> mapHospital { get; set; }
    public map<Id, map<Integer, list<ProductClass>>> mapOrder { get; set; }
    public Boolean PrintMode { get; set; }
    
    public ShippingSheetDetailBk(ApexPages.StandardController stdCtrl) {
      Account acc = (Account)stdCtrl.getRecord();
      PrintMode = (acc.Id!=null)?false:true;
      mapHospital = new map<Id, HospitalClass>();
      mapOrder = new map<Id, map<Integer, list<ProductClass>>>();
      String sql = 'Select Id,Hospital__c,Hospital__r.DateOfArrival01__c,Hospital__r.DateOfArrival02__c,Hospital__r.DateOfArrival03__c, ' +
      'Hospital__r.DateOfArrival04__c,Hospital__r.DateOfArrival05__c,Hospital__r.DateOfArrival06__c,Hospital__r.DateOfArrival07__c, ' +
      'Hospital__r.DateOfArrival08__c,Hospital__r.DateOfArrival09__c,Hospital__r.DateOfArrival10__c,Hospital__r.DateOfArrival11__c, ' +
      'Hospital__r.DateOfArrival12__c,Hospital__r.Name,Hospital__r.ShipmentDestinationCode__c,Hospital__r.BillingState,Hospital__r.BillingCity, ' +
      'Hospital__r.BillingStreet,Hospital__r.BillingPostalCode,Hospital__r.BillingAddress,Hospital__r.Phone,Hospital__r.Hcode__c, ' +
      'Staff__c,Staff__r.Name,OrderPrd1__c,OrderPrd1__r.SKU__c,OrderPrd1__r.Name,OrderPrd1__r.ProductType__c,OrderPrd2__c,OrderPrd2__r.SKU__c, ' +
      'OrderPrd2__r.ProductType__c,OrderPrd2__r.Name,OrderPrd3__c, OrderPrd3__r.ProductType__c,OrderPrd3__r.SKU__c,OrderPrd3__r.Name,' +
       'OrderPrd1Qty__c,OrderPrd2Qty__c,OrderPrd3Qty__c,EstDateOfArrival__c,Notes__c,EventParticipant__r.Event__r.Name From OrderShipping__c';
    if(acc != null) sql += ' Where Hospital__c = \'' + acc.Id + '\'';
    sql += ' Order By Staff__c,OrderPrd1__c,OrderPrd2__c,OrderPrd3__c';
    for(OrderShipping__c os : database.query(sql)) {
      if(os.Hospital__c == null) break;
      if(!mapHospital.containsKey(os.Hospital__c)) mapHospital.put(os.Hospital__c, new HospitalClass(os));
      if(!mapOrder.containsKey(os.Hospital__c)) 
        mapOrder.put(os.Hospital__c, new map<Integer, list<ProductClass>> { 1 => new list<ProductClass>() });
      map<Integer, list<ProductClass>> mapProduct = mapOrder.get(os.Hospital__c);
      Integer totalPageCount = mapProduct.size();
      list<ProductClass> listPrd = mapProduct.get(mapProduct.size());
      if(os.OrderPrd1__c != null && os.OrderPrd1Qty__c > 0) {
        ProductClass cls = new ProductClass();
        if(os.Staff__c != null) cls.StaffName = os.Staff__r.Name;
        cls.PrdSKU = os.OrderPrd1__r.SKU__c;
        cls.PrdName = os.OrderPrd1__r.Name;
        cls.PrdType = os.OrderPrd1__r.ProductType__c;
        cls.PrdQty = Integer.valueOf(os.OrderPrd1Qty__c);
        listPrd.add(cls);
      }
      if(os.OrderPrd2__c != null && os.OrderPrd2Qty__c > 0) {
        ProductClass cls = new ProductClass();
        if(os.Staff__c != null) cls.StaffName = os.Staff__r.Name;
        cls.PrdSKU = os.OrderPrd2__r.SKU__c;
        cls.PrdName = os.OrderPrd2__r.Name;
        cls.PrdType = os.OrderPrd2__r.ProductType__c;
        cls.PrdQty = Integer.valueOf(os.OrderPrd2Qty__c);
        listPrd.add(cls);
      }
      if(os.OrderPrd3__c != null && os.OrderPrd3Qty__c > 0) {
        ProductClass cls = new ProductClass();
        if(os.Staff__c != null) cls.StaffName = os.Staff__r.Name;
        cls.PrdSKU = os.OrderPrd3__r.SKU__c;
        cls.PrdName = os.OrderPrd3__r.Name;
        cls.PrdType = os.OrderPrd3__r.ProductType__c;
        cls.PrdQty = Integer.valueOf(os.OrderPrd3Qty__c);
        listPrd.add(cls);
      }
      mapProduct.put(mapProduct.size(), listPrd);
      mapOrder.put(os.Hospital__c,mapProduct);
    }
    }
    public void GoPrint() {
      PrintMode = true;
    }
    public class HospitalClass {
      public Id hId { get; set; }
      public String hName { get; set; }
      public String hCode { get; set; }
      public String ShipmentCode { get; set; }
      public String BillingState { get; set; }
      public String BillingCity { get; set; }
      public String BillingStreet { get; set; }
      public String BillingPostalCode { get; set; }
      public String ShipmentDate { get; set; }
      public String PhoneNumber { get; set; }
      public String EventName { get; set; }
      public HospitalClass(OrderShipping__c os) {
        hId = os.Hospital__c;
      hName = os.Hospital__r.Name;
      hCode = os.Hospital__r.Hcode__c;
      ShipmentCode = os.Hospital__r.ShipmentDestinationCode__c;
      BillingState = os.Hospital__r.BillingState;
      BillingCity = os.Hospital__r.BillingCity;
      BillingStreet = os.Hospital__r.BillingStreet;
      BillingPostalCode = os.Hospital__r.BillingPostalCode;
      PhoneNumber = os.Hospital__r.Phone;
      EventName = os.EventParticipant__r.Event__r.Name;
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival01__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival01__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival02__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival02__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival03__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival03__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival04__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival04__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival05__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival05__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival06__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival06__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival07__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival07__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival08__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival08__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival09__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival09__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival10__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival10__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival11__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival11__c);
      if(ShipmentDate == null && os.Hospital__r.DateOfArrival12__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival12__c);
      }
      public void calcShipmentDate(Date sDate) {
        if(Date.today().daysBetween(sDate) >= 0) 
          ShipmentDate = String.valueOf(sDate.year())+'年'+String.valueOf(sDate.month())+'月'+String.valueOf(sDate.day())+'日';
      }
    }
    public class ProductClass {
      public String StaffName { get; set; }
      public String PrdName { get; set; }
      public String PrdSKU { get; set; }
      public String PrdType { get; set; }
      public Integer PrdQty { get; set; }
    }
}