public with sharing class ShippingSheetPDF {
    public Account Hospital { get; set; }
    public Boolean IsLockSubmit { get; set; }
    // Hospital -> Event -> ShippingOrder
    public map<String, map<String, OrderShippingUtils.ShippingClass>> mapEventByHospital { get; set; }
    // Constructor
    public ShippingSheetPDF(ApexPages.StandardController stdCtrl) {
        Hospital = (Account)stdCtrl.getRecord();
        mapEventByHospital = OrderShippingUtils.calcOrderShipping(new list<String> { Hospital.Id });
        if(mapEventByHospital == null) ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Event Id is blank.'));
        IsLockSubmit = false;
    }
    public void InsertOrder() {
        OrderShippingUtils.InsertOrder(mapEventByHospital);
        IsLockSubmit = true;
    }
    public PageReference GoBack() {
        PageReference pRef = new PageReference('/'+Hospital.Id);
        pRef.setRedirect(true);
        return pRef;
    }
}