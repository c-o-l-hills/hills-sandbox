/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest 
public with sharing class MyLoginControllerTest {
    @IsTest
    public static void testMyLoginController() {
        list<ApexSwitch__c> listApexSwitch = CommUtils.insertApexTriggerSwitch();
        List<Event__c> eList = new List<Event__c>(); // Ev1: Overdue, Ev2: In Progress, Ev3: Not start, EV4: In Progress
        eList.add(new Event__c(Name='EV1',EventTypeInURL__c='event001',StartDate__c = date.today().addDays(-14),EndDate__c = date.today().addDays(-7)));
        eList.add(new Event__c(Name='EV2',EventTypeInURL__c='event002',StartDate__c = date.today().addDays(-1),EndDate__c = date.today().addDays(14)));
        eList.add(new Event__c(Name='EV3',EventTypeInURL__c='event003',StartDate__c = date.today().addDays(7),EndDate__c = date.today().addDays(30)));
        eList.add(new Event__c(Name='EV4',EventTypeInURL__c='event001',StartDate__c = date.today().addDays(-7),EndDate__c = date.today().addDays(7)));
        insert eList;
        
        Account acc = CommUtils.createHospital();
        Contact ctc = CommUtils.createStaff(acc);
        list<ProgramParticipant__c> listPP = new list<ProgramParticipant__c> { 
            new ProgramParticipant__c(Participant__c = ctc.Id,Event__c = eList[1].Id, userMail__c='user001@test.com'),
            new ProgramParticipant__c(Participant__c = ctc.Id,Event__c = eList[3].Id, userMail__c='user002@test.com')
        }; insert listPP;
        
        PageReference pageRef = page.mylogin;
        Test.setCurrentPage(pageRef);
        MyLoginController controller = new MyLoginController(); // No param
        
        Test.startTest();
        ApexPages.currentPage().getParameters().put('param', eList[1].EventTypeInURL__c);
        controller = new MyLoginController();
        controller.forwardToCustomAuthPage();
        controller.username = 'user001@test.com';
        controller.password = 'test1234';
        controller.login();
        controller.login2();
        controller.Register();
        
        ApexPages.currentPage().getParameters().put('param', eList[0].EventTypeInURL__c);
        controller = new MyLoginController();
        controller.login();
        controller.Register();
        
        ApexPages.currentPage().getParameters().put('param', eList[2].EventTypeInURL__c);
        controller = new MyLoginController();
        controller.login();
        controller.Register();
        
        ApexPages.currentPage().getParameters().put('param', eList[3].EventTypeInURL__c);
        controller = new MyLoginController();
        controller.username = 'user001@test.com';
        controller.password = 'test1234';
        controller.login();
        
        ApexPages.currentPage().getParameters().put('param', 'event999');
        controller = new MyLoginController();
        Test.stopTest();
        
        // 2017/04/04 入力ユーザIDとパスワードのブランク検知
        MyLoginController errorTrackController = new MyLoginController();
        // ケース１：ユーザ名前とパスワードはブランク
        ApexPages.getMessages().clear();
        errorTrackController.username = '';
        errorTrackController.password = '';
        errorTrackController.login();
        assertPageMessage(Label.USERNAME_IS_EMPTY);
        
        
        // ケース２：ユーザ名前はブランク
        ApexPages.getMessages().clear();
        errorTrackController.username = '';
        errorTrackController.password = 'password';
        errorTrackController.login();
        assertPageMessage(Label.USERNAME_IS_EMPTY);

        // ケース４：ユーザパスワードはブランク
        ApexPages.getMessages().clear();
        errorTrackController.username = 'username';
        errorTrackController.password = '';  
        errorTrackController.login();
        assertPageMessage(Label.PASSWORD_IS_EMPTY);
      
          
        // ケース５：入力ユーザIDとパスワード値あり
        // すでに応募完了のため、該当ケース不要 ASSERTしません。単純COVERAGE網羅ため
        errorTrackController.username = 'username';
        errorTrackController.password = 'password';
        errorTrackController.login();
        
        // その他のケース補足。ASSERTしません。COVERAGE網羅
        //存在しないパラメータ
        ApexPages.currentPage().getParameters().put('param', 'event00x');
        errorTrackController.login();

     }
     
     private static void assertPageMessage(String message) {
         List<Apexpages.Message> msgs = ApexPages.getMessages();
         boolean value = false;
         for(Apexpages.Message msg : msgs){
             if(msg.getDetail().contains(message))
                 value = true;
         }
         System.assert(value);
     }
}