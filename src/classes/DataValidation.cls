/**
 * データチェック用クラス
 */
public class DataValidation {
    public List<ApexPages.message> messages = new List<ApexPages.message>();
    
    public boolean hasError() {
        return messages.size() > 0;
    }
    
    // 必須チェック
    public void required(Integer min, Integer max, String regex, String itemName, String value) {
        if(value == null || value.length() == 0) {
            message('『' + itemName + '』を入力してください。');
        }else if(value.length() < min && min != -1) {
            message('『' + itemName + '』は' + min + '桁未満です。');
        }else if(value.length() > max && max != -1) {
            message('『' + itemName + '』は' + max + '桁超えです。');
        }else if(!Pattern.matches(regex, value)) {
            message('『' + itemName + '』の形式が正しくありません');
        }
    }
    
    public void required(Decimal min, String itemName, Decimal value) {
        if(value == null) {
            message('『' + itemName + '』を入力してください。');
        }else if(value < min) {
            message('『' + itemName + '』は' + min + 'より小さいです。');
        }
    }
    
    public void required(String itemName, Date value) {
        if(value == null) {
            message('『' + itemName + '』を入力してください。');
        }
    }
    
    private void message(String msg) {
        messages.add(new ApexPages.message(ApexPages.severity.ERROR, msg));
    }
}