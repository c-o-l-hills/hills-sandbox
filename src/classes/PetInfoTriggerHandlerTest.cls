@IsTest 
public class PetInfoTriggerHandlerTest {
    @IsTest
    public static void testBeforeInsert(){
		
        Account acc = CommUtils.createHospital();
       	Contact ctc = CommUtils.createStaff(acc);
       	list<ApexSwitch__c> listApexSwitch = CommUtils.insertApexTriggerSwitch();
       	User usr = CommUtils.createCommunityUser(ctc);
        list<ProductMaster__c> listPrd = CommUtils.createProducts();
       	Event__c ev = CommUtils.createEvent();
       	ProgramParticipant__c pp = CommUtils.createEventParticipant(ev,ctc,listPrd[0]);
	      CommUtils.createLogOnChatter(pp.Id,'Pet Food Calculation.');
       	
        System.runAs(usr) {
		        pp.Prd1_Ratio__c = 60; //ProductRegister2__c==null&&Prd1_Ratio__c!=null
	        	update pp;
	        	
	        	pp.ProductRegister2__c = listPrd[3].Id;
	        	update pp;
	        	
	        	pp.Prd1_Ratio__c = null;
	        	update pp;
	        	
	        	pp.PetType__c = 'Cat';
	        	pp.PetWeight__c = 4;
	        	pp.ProductRegister__c = listPrd[0].Id;
	        	update pp;
	        	
		        petInfo__c petInfo = new petInfo__c();
		        petInfo.RecordTypeId = CommUtils.getRecordTypeId('StaffFeeding','petInfo__c');
		        petInfo.attachMentIdAfter__c = 'attachMentIdAfter';
		        petInfo.isConfirmationToServiceTerms__c = true;
		        petInfo.EventParticipant__c = pp.Id;
		        petInfo.species__c = 'Dog';
		        petInfo.DietType__c = 'プリスクリプション・ダイエット';
		        petInfo.ProductTyoe__c = '犬 ドライ製品';
		        petInfo.ProductIsUsingNow__c = '犬h/d';
		        petInfo.bodyWeightAfter__c = 12.3;
		        insert petInfo;
		        CommUtils.RenderPDF(petInfo.Id);
        } 
    }
}