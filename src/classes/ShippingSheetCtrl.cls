public with sharing class ShippingSheetCtrl {
	/*
		抓取所有尚未出貨的製品訂購且已有納品日
	*/
	public Boolean PrintMode { get; set; }
	public String DeliveryMethod { get { return ApexPages.currentPage().getParameters().get('dm'); } }
	public list<ShippingInfoClass> listShippingInfo { get; set; }
	public list<ShippingInfoClass> listShippingPrint { get; set; }
	
	public ShippingSheetCtrl(ApexPages.StandardController stdCtrl) {
		Account acc = (Account)stdCtrl.getRecord();
		if(acc == null || acc.Id == null) return;
		//PrintMode = (acc!=null)?false:true;
		PrintMode = false;
		listShippingInfo = new list<ShippingInfoClass>();
		
		map<Integer, ShippingInfoClass> mapShippingInfo = new map<Integer, ShippingInfoClass>();
		for(OrderShipping__c os :  [Select Id,Hospital__c,Hospital__r.DateOfArrival01__c,Hospital__r.DateOfArrival02__c,Hospital__r.DateOfArrival03__c,
								Hospital__r.DateOfArrival04__c,Hospital__r.DateOfArrival05__c,Hospital__r.DateOfArrival06__c,Hospital__r.DateOfArrival07__c,
								Hospital__r.DateOfArrival08__c,Hospital__r.DateOfArrival09__c,Hospital__r.DateOfArrival10__c,Hospital__r.DateOfArrival11__c,
								Hospital__r.DateOfArrival12__c,Hospital__r.Name,Hospital__r.ShipmentDestinationCode__c,Staff__c,Staff__r.Name,DeliveryWays__c,
								OrderPrd1__c,OrderPrd1__r.SKU__c,OrderPrd1__r.Name,OrderPrd2__c,OrderPrd2__r.SKU__c,OrderPrd2__r.Name,OrderPrd3__c,
							 	OrderPrd3__r.SKU__c,OrderPrd3__r.Name,OrderPrd1Qty__c,OrderPrd2Qty__c,OrderPrd3Qty__c,Notes__c From OrderShipping__c
								Where Hospital__c =: acc.Id Order by DeliveryWays__c,OrderPrd1__c,OrderPrd2__c,OrderPrd3__c]) {
			if(os.DeliveryWays__c != null && DeliveryMethod != null && os.DeliveryWays__c != DeliveryMethod) break; 
			if(mapShippingInfo.size() == 0) mapShippingInfo.put(1, new ShippingInfoClass());
			ShippingInfoClass cls = mapShippingInfo.get(mapShippingInfo.size());
			//system.debug('Hospital->'+os.Hospital__r.Name+'+ShipCode->'+os.Hospital__r.ShipmentDestinationCode__c);
			if(String.isBlank(cls.ShipToCode)) cls.ShipToCode = os.Hospital__r.ShipmentDestinationCode__c;
			/* to get closest date from Today */
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival01__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival01__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival02__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival02__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival03__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival03__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival04__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival04__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival05__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival05__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival06__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival06__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival07__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival07__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival08__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival08__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival09__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival09__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival10__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival10__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival11__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival11__c);
			if(String.isBlank(cls.ShippingDate) && os.Hospital__r.DateOfArrival12__c != null) cls.formatShippingDate(os.Hospital__r.DateOfArrival12__c);
				
			if(os.OrderPrd1__c != null && os.OrderPrd1Qty__c > 0) {
				if(!cls.mapProduct.containsKey(os.OrderPrd1__c)) cls.mapProduct.put(os.OrderPrd1__c, new ProductClass());
				ProductClass prdCls = cls.mapProduct.get(os.OrderPrd1__c);
				if(String.isBlank(prdCls.PrdName))  prdCls.PrdName = os.OrderPrd1__r.Name;
				if(String.isBlank(prdCls.PrdSKU))  prdCls.PrdSKU = os.OrderPrd1__r.SKU__c;
				prdCls.OrderQty += Integer.valueOf(os.OrderPrd1Qty__c);
				cls.mapProduct.put(os.OrderPrd1__c, prdCls);
				if(cls.mapProduct.size() == 10) {
					mapShippingInfo.put(mapShippingInfo.size() + 1, new ShippingInfoClass(cls));
					cls = mapShippingInfo.get(mapShippingInfo.size());
				}
			} 
			if(os.OrderPrd2__c != null && os.OrderPrd2Qty__c > 0) {
				if(!cls.mapProduct.containsKey(os.OrderPrd2__c)) cls.mapProduct.put(os.OrderPrd2__c, new ProductClass());
				ProductClass prdCls = cls.mapProduct.get(os.OrderPrd2__c);
				if(String.isBlank(prdCls.PrdName))  prdCls.PrdName = os.OrderPrd2__r.Name;
				if(String.isBlank(prdCls.PrdSKU))  prdCls.PrdSKU = os.OrderPrd2__r.SKU__c;
				prdCls.OrderQty += Integer.valueOf(os.OrderPrd2Qty__c);
				cls.mapProduct.put(os.OrderPrd2__c, prdCls);
				if(cls.mapProduct.size() == 10) {
					mapShippingInfo.put(mapShippingInfo.size() + 1, new ShippingInfoClass(cls));
					cls = mapShippingInfo.get(mapShippingInfo.size());
				}
			}
			if(os.OrderPrd3__c != null && os.OrderPrd3Qty__c > 0) {
				if(!cls.mapProduct.containsKey(os.OrderPrd3__c)) cls.mapProduct.put(os.OrderPrd3__c, new ProductClass());
				ProductClass prdCls = cls.mapProduct.get(os.OrderPrd3__c);
				if(String.isBlank(prdCls.PrdName))  prdCls.PrdName = os.OrderPrd3__r.Name;
				if(String.isBlank(prdCls.PrdSKU))  prdCls.PrdSKU = os.OrderPrd3__r.SKU__c;
				prdCls.OrderQty += Integer.valueOf(os.OrderPrd3Qty__c);
				cls.mapProduct.put(os.OrderPrd3__c, prdCls);
				if(cls.mapProduct.size() == 10) {
					mapShippingInfo.put(mapShippingInfo.size() + 1, new ShippingInfoClass(cls));
					cls = mapShippingInfo.get(mapShippingInfo.size());
				}
			}
			mapShippingInfo.put(mapShippingInfo.size(), cls);
		}
		listShippingInfo = mapShippingInfo.values();
	}
	public void GoPrint() {
		listShippingPrint = new list<ShippingInfoClass>();
		for(ShippingInfoClass cls : listShippingInfo) {
			if(cls.GoPrint == true) listShippingPrint.add(cls);
		}
		PrintMode = true;
	}
    public class ShippingInfoClass {
    	public Boolean GoPrint { get; set; }
    	public String ShipToCode { get; set; } // 6 digits
    	public String Comment { get; set; } // 10 digits
    	public String ShippingDate { get; set; } // Format -> YYYYMMDD
    	public String Delv { get; set; } // 3 digits, unknown purpose
    	public map<String, ProductClass> mapProduct { get; set; }
    	public ShippingInfoClass() {
    		GoPrint = true; Comment = '';
    		mapProduct = new map<String, ProductClass>();
    	}
    	public ShippingInfoClass(ShippingInfoClass cls) {
    		ShipToCode = cls.ShipToCode;
    		ShippingDate = cls.ShippingDate;
    		Delv = cls.Delv;
    		GoPrint = true; Comment = '';
    		mapProduct = new map<String, ProductClass>();
    	}
    	public void formatShippingDate(Date sDate) {
    		if(Date.today().daysBetween(sDate) >= 0) 
    			ShippingDate = DateTime.newInstance(sDate,Time.newInstance(0,0,0,0)).format('YYYYMMdd');
    	}
    }
    public class ProductClass {
    	public String PrdName { get; set; }
    	public String PrdSKU { get; set; }
    	public Integer OrderQty { get; set; }
    	public ProductClass() {
    		OrderQty = 0;
    	}
    }
}