/**
 * ペット情報のトリガーハンドラークラス
 */
public class PetInfoTriggerHandler {
    
    /**
     * ペット情報のBefore Insert処理
     *
     * @param List<petInfo__c> petInfoList ペット情報リスト
     */
    public void processBeforeInsert (List<petInfo__c> petInfoList) {
        
        Id profileId = UserInfo.getProfileId();
        // 現在ユーザのプロファイルはHills_Custom_Commuity_UserまたはHills Company Communities Userの場合(ユーザがスタッフの場合)
        if (profileId == System.Label.HILLS_CUSTOM_COMMUITY_USER_ID || 
            profileId == System.Label.HILLS_COMPANY_COMMUNITIES_USER_ID ) {
            
            // 現在ユーザの関連スタッフ情報を検索する
            List<User> userContactList = [Select Id, ContactId From User Where Id = :UserInfo.getUserId() And ContactId != null];
            system.debug('----------------userContactList:'+userContactList);
            if (userContactList.size() == 0) return;
            
            for (petInfo__c petInfo : petInfoList) {
                petInfo.staff__c = userContactList[0].ContactId;      // 取引先責任者(スタッフ)
            }
            
        }

    }
    public void processMapBeforeInsert(map<Id,petInfo__c> oldMap, map<Id,petInfo__c> newMap) {
    }
}