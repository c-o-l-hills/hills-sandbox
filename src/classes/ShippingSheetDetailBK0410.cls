public with sharing class ShippingSheetDetailBK0410 {
    public Account acc { get; set; }
    public HospitalClass hospital { get; private set; }
    public String DeliveryMethod { get { return ApexPages.currentPage().getParameters().get('dm'); } }
    public map<Integer, list<ProductClass>> mapProduct { get; private set; }
    public list<OrderShipping__c> listHonSyaOrder { get; set; }
    public list<OrderShipping__c> listBessoOrder { get; set; }
    /* to count total pages by pre-defined row number a page. */
    public Integer totalPageCount { get; private set; }
    private Integer PAGE1_ROWS;
    private Integer PAGE_ROWS;
    
    public ShippingSheetDetailBK0410(ApexPages.StandardController stdCtrl) {
        acc = (Account)stdCtrl.getRecord();
        if(acc == null || acc.Id == null) return;
        for(OrderPrintSetting__c oSetting : [Select Name,rows_a_page__c From OrderPrintSetting__c]) {
        		if(oSetting.Name == 'Page1') PAGE1_ROWS = Integer.valueOf(oSetting.rows_a_page__c);
        		if(oSetting.Name == 'Page Other') PAGE_ROWS = Integer.valueOf(oSetting.rows_a_page__c);
        }
    }
    public void CalcDetail() {
        mapProduct = new map<Integer, list<ProductClass>>();
        
        String sql = 'Select Id,Hospital__c,Hospital__r.DateOfArrival01__c,Hospital__r.DateOfArrival02__c,Hospital__r.DateOfArrival03__c, ' +
            'Hospital__r.DateOfArrival04__c,Hospital__r.DateOfArrival05__c,Hospital__r.DateOfArrival06__c,Hospital__r.DateOfArrival07__c, ' +
            'Hospital__r.DateOfArrival08__c,Hospital__r.DateOfArrival09__c,Hospital__r.DateOfArrival10__c,Hospital__r.DateOfArrival11__c, ' +
            'Hospital__r.DateOfArrival12__c,Hospital__r.Name,Hospital__r.ShipmentDestinationCode__c,Hospital__r.BillingState,Hospital__r.BillingCity, ' +
            'Hospital__r.BillingStreet,Hospital__r.BillingPostalCode,Hospital__r.BillingAddress,Hospital__r.Phone,Hospital__r.Hcode__c, ' +
            'Staff__c,Staff__r.Name,OrderPrd1__c,Prd1_SKU__c,ProductName1__c,OrderPrd1__r.ProductType__c,OrderPrd2__c,Prd2_SKU__c, ' +
            'OrderPrd2__r.ProductType__c,ProductName2__c,OrderPrd3__c, OrderPrd3__r.ProductType__c,Prd3_SKU__c,ProductName3__c,' +
            'OrderPrd1Qty__c,OrderPrd2Qty__c,OrderPrd3Qty__c,Notes__c, EventParticipant__r.Event__r.event_name_pdf__c,Hospital__r.ShipSheetCode__c ' +
            'From OrderShipping__c';
        sql += ' Where Hospital__c = \'' + acc.Id + '\' And WillShip__c = true';
        if(String.isNotBlank(DeliveryMethod)) sql += ' And DeliveryWays__c = \'' + DeliveryMethod + '\'';
        sql += ' Order By OrderPrd1__r.Name,OrderPrd2__r.Name,OrderPrd3__r.Name,Staff__c';
        totalPageCount = 1;
        mapProduct.put(totalPageCount, new list<ProductClass>());
        for(OrderShipping__c os : database.query(sql)) {
            if(os.Hospital__c == null) break;
            if(hospital == null) hospital = new HospitalClass(os);
            if(hospital.ShipmentDateOnHospital == null || hospital.ShipmentDate == null) return;
            
            if(os.OrderPrd1__c != null && os.OrderPrd1Qty__c > 0) {
                ProductClass cls = new ProductClass();
                if(os.Staff__c != null) cls.StaffName = os.Staff__r.Name;
                cls.PrdSKU = os.Prd1_SKU__c;
                cls.PrdName = os.ProductName1__c;
                cls.PrdType = os.OrderPrd1__r.ProductType__c;
                cls.PrdQty = Integer.valueOf(os.OrderPrd1Qty__c);
                if ((totalPageCount==1 && mapProduct.get(totalPageCount).size()<PAGE1_ROWS) || 
                    (totalPageCount<>1 && mapProduct.get(totalPageCount).size()<PAGE_ROWS)) {
                    mapProduct.get(totalPageCount).add(cls);
                } else {
                    totalPageCount = totalPageCount + 1;
                    list<ProductClass> listCls = new list<ProductClass>();
                    listCls.add(cls);
                    mapProduct.put(totalPageCount, listCls);
                }
            }
            if(os.OrderPrd2__c != null && os.OrderPrd2Qty__c > 0) {
                ProductClass cls = new ProductClass();
                if(os.Staff__c != null) cls.StaffName = os.Staff__r.Name;
                cls.PrdSKU = os.Prd2_SKU__c;
                cls.PrdName = os.ProductName2__c;
                cls.PrdType = os.OrderPrd2__r.ProductType__c;
                cls.PrdQty = Integer.valueOf(os.OrderPrd2Qty__c);
                
                if ((totalPageCount==1 && mapProduct.get(totalPageCount).size()<PAGE1_ROWS) || 
                    (totalPageCount<>1 && mapProduct.get(totalPageCount).size()<PAGE_ROWS)) {
                    mapProduct.get(totalPageCount).add(cls);
                } else {
                    totalPageCount = totalPageCount + 1;
                    list<ProductClass> listCls = new list<ProductClass>();
                    listCls.add(cls);
                    mapProduct.put(totalPageCount, listCls);
                }
            }
            if(os.OrderPrd3__c != null && os.OrderPrd3Qty__c > 0) {
                ProductClass cls = new ProductClass();
                if(os.Staff__c != null) cls.StaffName = os.Staff__r.Name;
                cls.PrdSKU = os.Prd3_SKU__c;
                cls.PrdName = os.ProductName3__c;
                cls.PrdType = os.OrderPrd3__r.ProductType__c;
                cls.PrdQty = Integer.valueOf(os.OrderPrd3Qty__c);
                
                if ((totalPageCount==1 && mapProduct.get(totalPageCount).size()<PAGE1_ROWS) || 
                    (totalPageCount<>1 && mapProduct.get(totalPageCount).size()<PAGE_ROWS)) {
                    mapProduct.get(totalPageCount).add(cls);
                } else {
                    totalPageCount = totalPageCount + 1;
                    list<ProductClass> listCls = new list<ProductClass>();
                    listCls.add(cls);
                    mapProduct.put(totalPageCount, listCls);
                }
            }
        }
        
        if (totalPageCount == 1 && mapProduct.get(1).size()<PAGE1_ROWS) {
            for (Integer i = mapProduct.get(1).size(); i < PAGE1_ROWS; i++) {
                ProductClass cls = new ProductClass();
                mapProduct.get(1).add(cls);
            }
        } else if (totalPageCount > 1 && mapProduct.get(totalPageCount).size()<PAGE_ROWS){
            for (Integer i = mapProduct.get(totalPageCount).size(); i < PAGE_ROWS; i++) {
                ProductClass cls = new ProductClass();
                mapProduct.get(totalPageCount).add(cls);
            }
        }
    }
    public void GoPrint() {
        //PrintMode = true;
    }
    public void CreateLog() {
        if(acc != null && acc.Id != null) {
            CalcDetail();
            if(hospital != null) {
                Account updateAcc = [Select Id,EstDateOfArrival__c,ShipSheetPrintCount__c From Account Where Id =: acc.Id];
                if(updateAcc.ShipSheetPrintCount__c == null) updateAcc.ShipSheetPrintCount__c = 1; else updateAcc.ShipSheetPrintCount__c += 1; 
                if(hospital.ShipmentDateOnHospital != null) updateAcc.EstDateOfArrival__c = hospital.ShipmentDateOnHospital;
                update updateAcc;
                FeedItem feedLog = CommUtils.createLogOnChatter(acc.Id,System.Label.HonSyaShippingPrint+','+DateTime.now());
                insert feedLog;
            }
        }
    }
    public class HospitalClass {
        public Id hId { get; set; }
        public String ShipSheetCode { get; set; }
        public String ShipmentCode { get; set; }
        public Date ShipmentDateOnHospital { get; set; }
        public String ShipmentDate { get; set; }
        public String EventName { get; set; }
        public HospitalClass(OrderShipping__c os) {
            hId = os.Hospital__c;
            ShipSheetCode = os.Hospital__r.ShipSheetCode__c;
            ShipmentCode = os.Hospital__r.ShipmentDestinationCode__c;
            EventName = os.EventParticipant__r.Event__r.event_name_pdf__c;
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival01__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival01__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival02__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival02__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival03__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival03__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival04__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival04__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival05__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival05__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival06__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival06__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival07__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival07__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival08__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival08__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival09__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival09__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival10__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival10__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival11__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival11__c);
            if(ShipmentDate == null && os.Hospital__r.DateOfArrival12__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival12__c);
        }
        public void calcShipmentDate(Date sDate) {
            if(Date.today().daysBetween(sDate) >= 0) {
                ShipmentDateOnHospital = sDate;
                ShipmentDate = String.valueOf(sDate.year())+'年'+String.valueOf(sDate.month())+'月'+String.valueOf(sDate.day())+'日';
            }
        }
    }
    public class ProductClass {
        public String StaffName { get; set; }
        public String PrdName { get; set; }
        public String PrdSKU { get; set; }
        public String PrdType { get; set; }
        public Integer PrdQty { get; set; }
    }
}