@isTest public class MyPageControllerTest{
@isTest
public static void myPageControllerTest(){
    
    MyPageController controller = new MyPageController();
    List<SelectOption> options = controller.eventOptios;
    Test.startTest();
    controller.resetPageQues();
    controller.goToMyPage();
    controller.forwardToCustomAuthPage();
    controller.upsertData();
    
    Account hospital = CommUtils.createHospital();
    Contact staff = CommUtils.createStaff(hospital);
    Event__c ev = CommUtils.createEvent();
    list<ProductMaster__c> listPrd = CommUtils.createProducts();
    list<ApexSwitch__c> listApexSwitch = CommUtils.insertApexTriggerSwitch();
    
    ProgramParticipant__c ProgramParticipant = CommUtils.createEventParticipant(ev,staff,listPrd[0]);
    
    controller.newPetInfo.ProductIsUsingNow__c = '体重管理';
    controller.eType = 'etpye';
    controller.qType = 'qtpye';
    
    petInfo__c newPetInfo = new petInfo__c();
    newPetInfo.isConfirmationToServiceTerms__c = true;
    newPetInfo.EventParticipant__c = ProgramParticipant.id;
    Database.insert(newPetInfo);
    
    controller.resetPageQues();
    controller.goToMyPage();
    controller.forwardToCustomAuthPage();
    
   
    
    MyPageController.doSubmit(null, null, null, null, null, null);
    MyPageController.doSubmit(newPetInfo.ID, null, null, null, null, null);
    //MyPageController.doSubmit('a03O000000I1HHZIA3', null, null, null, null, null);
    MyPageController.doSubmit(newPetInfo.ID, 'Body', 'Bill', null, null, null);
    MyPageController.doSubmit(newPetInfo.ID, 'Body', 'Bill', 'Body1', 'Rose', null);
    
    Test.stopTest();
    
    }

}