/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class MyLoginControllerBK {
    
    private Static final String EventTypeInURL_1 = 'event001';
    private Static final String EventTypeInURL_2 = 'event002';
    
    global String username { get; set; }
    global String password { get; set; }
    global String pagePara { get; set; }
    public String EventTypeInURL { get; set; }
    public String PageName { get; set; }
    
    global MyLoginControllerBK() {
        PageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        // パラメータが'event001'、'event002'以外の場合、パラメータエラーメッセージを表示する。
        if (EventTypeInURL != EventTypeInURL_1 &&
            EventTypeInURL != EventTypeInURL_2) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.LOGIN_PAGE_PARA_ERR_MSG));
        }
    }
    global PageReference chkIfStaff() {
        /**
        String myStaffEventStr = ApexPages.currentPage().getParameters().get('myStaffEventStr');
        
        // get staff info
        List<ProgramParticipant__c > pList = [SELECT Id,
                                              staffId__c ,
                                              EventType__c,
                                              ParticipantUser__c ,
                                              account__c 
                                              FROM
                                              ProgramParticipant__c 
                                              WHERE
                                              Id = :myStaffEventStr
                                             ];
        
        if(pList.size()==0){
            // 参加者対象外 TODO 
            return null;
        }
        pagePara = myStaffEventStr;
        /** TODO
        if(pList.get(0).ParticipantUser__c == null){
            // 初回として、登録ページへ遷移します。
            PageReference pageRef = Page.register;
            pageRef.getParameters().put('myStaffEventStr',myStaffEventStr);
            return PageRef;
        }
*/
        return null;
    }
    global PageReference forwardToCustomAuthPage() {
        return Page.Mylogin;
    }
    global PageReference login() {
        
        EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        system.debug('●●●●●●eType_login' + EventTypeInURL);
        if(String.isNotBlank(EventTypeInURL)){
            // get event info
            List<Event__c> eList = [SELECT Id FROM Event__c 
              WHERE EventTypeInURL__c = :EventTypeInURL And IsOverdue__c = false LIMIT 1 ];
            
            if(eList.size() >0 ) {
                String AfterLoginPage;
                //スタッフフィーディング：
                //https://[sandbox domain]/a/event001
                //ログイン→　マイページ（スタッフフィーディング）
                if(EventTypeInURL == EventTypeInURL_1) {
                    AfterLoginPage = page.MyPage.getUrl(); 
                    //return Site.login(username, password, page.MyPage.getUrl() + ('?eType='+eType));
                } 
                //症例収集：
                //https://[sandbox domain]/a/event002
                //ログイン→　マイページ（症例収集）
                if(EventTypeInURL == EventTypeInURL_2) {
                    AfterLoginPage = Page.MyPageCaseCollection.getUrl();
                    //return Site.login(username, password, page.MyPageCaseCollection.getUrl() + ('?eType='+eType));
                }
                List<ProgramParticipant__c> ParticipantList = [Select Id, Name From ProgramParticipant__c 
                                                                Where Event__r.EventTypeInURL__c = :EventTypeInURL AND userMail__c = :username];
                if(ParticipantList.size() == 0){
                    if(String.isNotBlank(AfterLoginPage)){
                        return Site.login(username, password, AfterLoginPage + '?eType=' + eList.get(0).Id);
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Event_Participation));
                    return null;
                }
            }
        }
        // 切替テスト
        //return Site.login(username, password, page.SelectEventPage.getUrl() );
        //return Site.login(username, password, page.MyPage.getUrl() + '?eType=a00O000000ZPZui');
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.URLERRORMESSAGE));
        return null;
        //PageReference p = new PageReference('/myLogin');
        //p.setRedirect(true);
        //return p;
        // 症例収集
        //return Site.login(username, password, page.MyPageCaseCollection.getUrl() + '?eType=a00O000000ZPZui' );
        
    }
    global PageReference login2() {
        // 症例収集
        return Site.login(username, password, page.MyPageCaseCollection.getUrl() + '?eType=a00O000000ZRdLt' );
    }
    global PageReference Register() {
        String EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        if(EventTypeInURL == EventTypeInURL_1) {
            PageReference p = page.Register;
            p.getParameters().put('param', EventTypeInURL_1);
            p.setRedirect(true);
            return p;
        }
        if(EventTypeInURL == EventTypeInURL_2) {
            PageReference p = page.Register;
            p.getParameters().put('param', EventTypeInURL_2);
            p.setRedirect(true);
            return p;
        }
        return null;
    }
    
    
}