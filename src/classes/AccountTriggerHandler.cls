/**
 * 病院のトリガーハンドラークラス
 */
public class AccountTriggerHandler {
    
    /**
     * 病院のAfter Update処理
     *
     * @param Map<Id, Account> accMap 病院マップ
     */
    public static void processAfterUpdate (Map<Id, Account> accMap) {
        
        // 退会病院に紐づいている、かつ退会していないスタッフを検索する
        List<Account> accList = [Select Id, (Select Id, Status__c From Contacts Where Status__c != :System.Label.STATUS_WITHDRAWAL) 
            From Account Where Id In :accMap.keySet() And Status__c = :System.Label.STATUS_WITHDRAWAL];
        
        List<Contact> conList = new List<Contact>();
        // 病院が退会の場合、病院に紐づいているスタッフも退会する
        for (Account acc : accList ) {
            for (Contact con : acc.Contacts) {
                con.Status__c = System.Label.STATUS_WITHDRAWAL;
                conList.add(con);
            }
        }
        if (conList.size() > 0) update conList;
    }

}