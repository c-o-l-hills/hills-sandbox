global without sharing class OrderShippingUtils {
    webService static Boolean calHospitalOrder(String hId) {
        map<String, map<String, ShippingClass>> mapEventByHospital = calcOrderShipping(new list<String> { hId });
        return (mapEventByHospital.size()>0)?true:false;
    }
    public static map<Id, RecordType> mapPrdRecordType { get {
        return new map<Id, RecordType>([Select Id,Name,DeveloperName From RecordType Where SObjectType='ProductMaster__c']);
    } }
    // Make Order Shipping Calculation. Hospital -> Event -> Order Product
    public static map<String, map<String, ShippingClass>> calcOrderShipping(list<String> HospitalIds) {
        map<String, map<String, ShippingClass>> mapEventByHospital = new map<String, map<String, ShippingClass>>();
        // Put all Order into list by Product
        for(OrderShipping__c os : [Select Hospital__r.DateOfArrival01__c,Hospital__r.DateOfArrival02__c,Hospital__r.DateOfArrival03__c,
            Hospital__r.DateOfArrival04__c,Hospital__r.DateOfArrival05__c,Hospital__r.DateOfArrival06__c,Hospital__r.DateOfArrival07__c,
            Hospital__r.DateOfArrival08__c,Hospital__r.DateOfArrival09__c,Hospital__r.DateOfArrival10__c,Hospital__r.DateOfArrival11__c,
            Hospital__r.DateOfArrival12__c,Hospital__c,Hospital__r.ShipSheetPrintCount__c,Hospital__r.ShipSheetCode__c,
            OrderPrd1__c,OrderPrd1__r.PacksInBox__c,Prd1_SKU__c,ProductName1__c,OrderPrd1__r.ProductFamily__c,OrderPrd1Qty__c,
            OrderPrd1__r.RecordTypeId,OrderPrd1__r.ProductType__c,
            OrderPrd2__c,OrderPrd2__r.PacksInBox__c,Prd2_SKU__c,ProductName2__c,OrderPrd2__r.ProductFamily__c,OrderPrd2Qty__c,
            OrderPrd2__r.RecordTypeId,OrderPrd2__r.ProductType__c,
            OrderPrd3__c,OrderPrd3__r.PacksInBox__c,Prd3_SKU__c,ProductName3__c,OrderPrd3__r.ProductFamily__c,OrderPrd3Qty__c,
            OrderPrd3__r.ProductType__c,OrderPrd3__r.RecordTypeId,
			Staff__c,Staff__r.Name,EventParticipant__r.Event__c,EventParticipant__r.Event__r.Name
			From OrderShipping__c Where Hospital__c =: HospitalIds And WillShip__c = true 
			Order by ProductName1__c,OrderPrd1Qty__c DESC,ProductName2__c,OrderPrd2Qty__c DESC,ProductName3__c,OrderPrd3Qty__c DESC]) {
			String EventId = os.EventParticipant__r.Event__c;
			if(EventId == null) continue;
			// Store Hospital Information for PDF
			if(!mapEventByHospital.containsKey(os.Hospital__c))
			    mapEventByHospital.put(os.Hospital__c,new map<String,ShippingClass>());
			map<String,ShippingClass> mapEvent = mapEventByHospital.get(os.Hospital__c);
			if(!mapEvent.containsKey(EventId)) mapEvent.put(EventId,new ShippingClass(os));
			ShippingClass sCls = mapEvent.get(EventId);
			// put prd1, prd2, prd3 into one list
			if(String.isNotBlank(os.OrderPrd1__c)) {
    			if(!sCls.mapAllOrder.containsKey(os.OrderPrd1__c)) sCls.mapAllOrder.put(os.OrderPrd1__c,new list<OrderClass>());
    			sCls.mapAllOrder.get(os.OrderPrd1__c).add(new OrderClass(os.Staff__c,os.Staff__r.Name,os.OrderPrd1Qty__c,os.OrderPrd1__c));
    			// Store Product Information
    			if(!sCls.mapPrdInfo.containsKey(os.OrderPrd1__c)) sCls.mapPrdInfo.put(os.OrderPrd1__c,new ProductClass(os.OrderPrd1__r.RecordTypeId,
    			    os.OrderPrd1__c,os.ProductName1__c,os.Prd1_SKU__c,os.OrderPrd1__r.ProductFamily__c,os.OrderPrd1__r.ProductType__c,
    			    os.OrderPrd1__r.PacksInBox__c));
			}
			if(String.isNotBlank(os.OrderPrd2__c)) {
    			if(!sCls.mapAllOrder.containsKey(os.OrderPrd2__c)) sCls.mapAllOrder.put(os.OrderPrd2__c,new list<OrderClass>());
    			sCls.mapAllOrder.get(os.OrderPrd2__c).add(new OrderClass(os.Staff__c,os.Staff__r.Name,os.OrderPrd2Qty__c,os.OrderPrd2__c));
    			// Store Product Information
    			if(!sCls.mapPrdInfo.containsKey(os.OrderPrd2__c)) sCls.mapPrdInfo.put(os.OrderPrd2__c, new ProductClass(os.OrderPrd2__r.RecordTypeId,
			        os.OrderPrd2__c,os.ProductName2__c,os.Prd2_SKU__c,os.OrderPrd2__r.ProductFamily__c,os.OrderPrd2__r.ProductType__c,
			        os.OrderPrd2__r.PacksInBox__c));
			}
			if(String.isNotBlank(os.OrderPrd3__c)) {
    			if(!sCls.mapAllOrder.containsKey(os.OrderPrd3__c)) sCls.mapAllOrder.put(os.OrderPrd3__c,new list<OrderClass>());
    			sCls.mapAllOrder.get(os.OrderPrd3__c).add(new OrderClass(os.Staff__c,os.Staff__r.Name,os.OrderPrd3Qty__c,os.OrderPrd3__c));
    			// Store Product Information
    			if(!sCls.mapPrdInfo.containsKey(os.OrderPrd3__c)) sCls.mapPrdInfo.put(os.OrderPrd3__c, new ProductClass(os.OrderPrd3__r.RecordTypeId,
			        os.OrderPrd3__c,os.ProductName3__c,os.Prd3_SKU__c,os.OrderPrd3__r.ProductFamily__c,os.OrderPrd3__r.ProductType__c,
			        os.OrderPrd3__r.PacksInBox__c));
			} 
			mapEvent.put(EventId, sCls); mapEventByHospital.put(os.Hospital__c,mapEvent);
		}
		for(String hId : mapEventByHospital.keySet()) {
    	    for(String eId : mapEventByHospital.get(hId).keySet()) {
    	        ShippingClass sCls = mapEventByHospital.get(hId).get(eId); //system.debug(shippingCls);
    	        sCls.CalcOrderDelivery();
    	    }
		}
    	return mapEventByHospital;
    }
    public static void InsertOrder(map<String, map<String, ShippingClass>> mapEventByHospital) {
        // Dispatch products to besso / honsya
		map<Id, Account> mapHospital = new map<Id, Account>();
		list<OrderShipping__c> listNewOrder = new list<OrderShipping__c>();
    	for(String hId : mapEventByHospital.keySet()) {
    	    for(String eId : mapEventByHospital.get(hId).keySet()) {
    	        ShippingClass sCls = mapEventByHospital.get(hId).get(eId); //system.debug(shippingCls);
    	        //sCls.CalcOrderDelivery();
    	        mapHospital.put(sCls.Hospital.Id,sCls.Hospital);
    	        listNewOrder.addAll(sCls.listOrderQty);
    	    } // End of Event loop
    	} // End of Hospital loop
    	list<OrderShipping__c> listOldOrder = [Select Id,IsLatest__c From OrderShipping__c Where IsLatest__c=true 
                And ShippingSheetCDT__c<>null And CreatePurpose__c <> null And Hospital4Shipping__c=:mapHospital.keySet()];
        if(listOldOrder.size() > 0) { for(OrderShipping__c os : listOldOrder) os.IsLatest__c = false; update listOldOrder; }
        if(listNewOrder.size() > 0) { system.debug(listNewOrder); insert listNewOrder; update mapHospital.values(); }
    }
    public class ShippingClass {
        public Integer PrdShippingCountPerRow { get { return 10; } }
        public Id EventId { get; set; }
        public String EventName { get; set; }
        public Account Hospital { get; set; }
        public String SheetPrintCode { get; set; }
        public Date ShipmentDateOnHospital { get; set; }
        public map<String, list<OrderClass>> mapAllOrder { get; set; }
        public map<String, list<OrderClass>> mapBessoOrder { get; set; }
        public map<String, list<OrderClass>> mapHonsyaOrder { get; set; }
        public map<String, map<String, OrderClass>> mapOrderDmQty { get; set; }
        public map<String, map<String, OrderClass>> mapStaffBessoOrder { get; set; }
        public map<String, map<String, OrderClass>> mapStaffHonsyaOrder { get; set; }
        public map<String, ProductClass> mapPrdInfo { get; set; }
        public list<OrderShipping__c> listOldOrder { get; set; }
        public list<OrderShipping__c> listOrderQty { get; set; }
        public ShippingClass(OrderShipping__c os) {
            EventId = os.EventParticipant__r.Event__c;
            EventName = os.EventParticipant__r.Event__r.Name;
            Integer NextPrintCount = Integer.valueOf(os.Hospital__r.ShipSheetPrintCount__c) + 1;
            SheetPrintCode = os.Hospital__r.ShipSheetCode__c.substring(0,
                os.Hospital__r.ShipSheetCode__c.LastIndexOf('-') + 1) + String.valueOf(NextPrintCount);
            Hospital = new Account(Id=os.Hospital__c,ShipSheetPrintCount__c=NextPrintCount);
            if(os.Hospital__r.DateOfArrival01__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival01__c);
            if(os.Hospital__r.DateOfArrival02__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival02__c);
            if(os.Hospital__r.DateOfArrival03__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival03__c);
            if(os.Hospital__r.DateOfArrival04__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival04__c);
            if(os.Hospital__r.DateOfArrival05__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival05__c);
            if(os.Hospital__r.DateOfArrival06__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival06__c);
            if(os.Hospital__r.DateOfArrival07__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival07__c);
            if(os.Hospital__r.DateOfArrival08__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival08__c);
            if(os.Hospital__r.DateOfArrival09__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival09__c);
            if(os.Hospital__r.DateOfArrival10__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival10__c);
            if(os.Hospital__r.DateOfArrival11__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival11__c);
            if(os.Hospital__r.DateOfArrival12__c != null) calcShipmentDate(os.Hospital__r.DateOfArrival12__c);
            // ProductId -> Orders
            mapAllOrder = new map<String, list<OrderClass>>();
            mapBessoOrder = new map<String, list<OrderClass>>();
            mapHonsyaOrder = new map<String, list<OrderClass>>();
            mapPrdInfo = new map<String, ProductClass>();
            // DeliveryWay -> PrdId ->Order
            mapOrderDmQty = new map<String, map<String, OrderClass>> { 
                '本社'=>new map<String, OrderClass>(),
                '別送'=>new map<String, OrderClass>()
            };
            // Staff -> Product -> Order
            mapStaffBessoOrder = new map<String, map<String, OrderClass>>();
            mapStaffHonsyaOrder = new map<String, map<String, OrderClass>>();
            
            listOrderQty = new list<OrderShipping__c>();
        }
        public void calcShipmentDate(Date sDate) {
            if(Date.today().daysBetween(sDate) >= 0) Hospital.EstDateOfArrival__c = sDate;
        }
        public void CalcOrderDelivery() {
            for(String oKey : mapAllOrder.keySet()) {
    		    Integer QtyPerPack = mapPrdInfo.get(oKey).QtyPerPack;
    		    String prdName = mapPrdInfo.get(oKey).PrdName;
    		    Integer totalQty = 0; Integer recNum = 0;
    		    //system.debug(prdName+'->All Orders:'+mapAllOrder.get(oKey)+'->'+QtyPerPack);
    		    
    		    list<OrderClass> listOrder = new list<OrderClass>();
    		    for(OrderClass oCls : mapAllOrder.get(oKey)) {
    		        // 如果目前訂單數量小於製品每箱數量
    		        if(totalQty < QtyPerPack) { //system.debug('Current:'+totalQty+':'+oCls.OrderQty);
    		            totalQty += oCls.OrderQty;
    		            listOrder.add(new OrderClass(oCls.StaffId,oCls.StaffName,oCls.OrderQty,oCls.OrderPrdId));
    		        } //system.debug(prdName+'->Current Qty:'+totalQty+'->Order:'+listOrder);
    		        if(totalQty >= QtyPerPack) {
    		            Integer quotient = totalQty / QtyPerPack;
    		            Integer remainder = totalQty - quotient * QtyPerPack;
    		            listOrder[listOrder.size()-1].OrderQty -= remainder;
    		            if(!mapBessoOrder.containsKey(oKey)) mapBessoOrder.put(oKey,new list<OrderClass>());
    		            for(OrderClass oc : listOrder) oc.DeliveryWay = '別送';
    		            mapBessoOrder.get(oKey).addAll(new list<OrderClass>(listOrder));
    		            // Empty temp order list
    		            listOrder = new list<OrderClass>();
    		            if(remainder > 0) listOrder.add(new OrderClass(oCls.StaffId,oCls.StaffName,remainder,oCls.OrderPrdId));
    		            //system.debug(prdName+':'+totalQty+':'+quotient*QtyPerPack+':'+remainder+':'+listOrder.size()+':'+mapBessoOrder.size()); 
    		            totalQty = remainder; 
    		        } //system.debug('Total Qty:'+totalQty+'->Order List:'+listOrder.size()+'->Record:'+recNum);
    		        // 如果是最後一筆數據
    		        if(recNum == mapAllOrder.get(oKey).size() - 1) { //system.debug('Last of Product:'+mapPrdInfo.get(oKey).PrdSKU);
    		            if(!mapHonsyaOrder.containsKey(oKey)) mapHonsyaOrder.put(oKey,new list<OrderClass>());
    		            for(OrderClass oc : listOrder) oc.DeliveryWay = '本社';
    		            mapHonsyaOrder.get(oKey).addAll(new list<OrderClass>(listOrder));
    		        } else recNum++;
    		    } //system.debug(prdName+'->Add to Besso Order list:'+listBessoOrder); // End of OrderClass loop by Product
    		} // End of All Order loop
    		for(String oKey : mapBessoOrder.keySet()) { // 計算製品數量
		        for(OrderClass oCls : mapBessoOrder.get(oKey)) {
		            map<String, OrderClass> mapOrderQty = mapOrderDmQty.get('別送');
		            if(!mapOrderQty.containsKey(oKey)) mapOrderQty.put(oKey, new OrderClass(oCls));
		            else mapOrderQty.get(oKey).OrderQty += oCls.OrderQty;
		            // Calculate qty by Staff
		            if(!mapStaffBessoOrder.containsKey(oCls.StaffId))
		                mapStaffBessoOrder.put(oCls.StaffId,new map<String,OrderClass>());
                    map<String,OrderClass> mapPrdOrder = mapStaffBessoOrder.get(oCls.StaffId);
                    if(!mapPrdOrder.containsKey(oKey)) mapPrdOrder.put(oKey,new OrderClass(oCls));
                    else mapPrdOrder.get(oKey).OrderQty += oCls.OrderQty;
		        }
            }  
            for(String oKey : mapHonsyaOrder.keySet()) { // 計算製品數量
		        for(OrderClass oCls : mapHonsyaOrder.get(oKey)) {
		            map<String, OrderClass> mapOrderQty = mapOrderDmQty.get('本社');
		            if(!mapOrderQty.containsKey(oKey)) mapOrderQty.put(oKey, new OrderClass(oCls));
		            else mapOrderQty.get(oKey).OrderQty += oCls.OrderQty;
		            //
		            if(!mapStaffHonsyaOrder.containsKey(oCls.StaffId))
		                mapStaffHonsyaOrder.put(oCls.StaffId,new map<String,OrderClass>());
                    map<String,OrderClass> mapPrdOrder = mapStaffHonsyaOrder.get(oCls.StaffId);
                    if(!mapPrdOrder.containsKey(oKey)) mapPrdOrder.put(oKey,new OrderClass(oCls));
                    else mapPrdOrder.get(oKey).OrderQty += oCls.OrderQty;
		        }
            }
            DateTime CurrentDT = DateTime.now();
            Hospital.ShippingSheetCDT__c = CurrentDT;
            //出荷指示書。Prd Type -> Order
            map<String, map<String, map<Integer, OrderShipping__c>>> mapDmPrdTypeOrder = new map<String, map<String, map<Integer, OrderShipping__c>>> { 
                '本社' => new map<String, map<Integer, OrderShipping__c>> {
                    'PD' => new map<Integer, OrderShipping__c> { 1 => new OrderShipping__c() }, 
                    'SD' => new map<Integer, OrderShipping__c> { 1 => new OrderShipping__c() }
                }, '別送' => new map<String, map<Integer, OrderShipping__c>> {
                    'PD' => new map<Integer, OrderShipping__c> { 1 => new OrderShipping__c() }, 
                    'SD' => new map<Integer, OrderShipping__c> { 1 => new OrderShipping__c() }
                }
            };
            for(String dm : mapOrderDmQty.keySet()) {
                for(OrderClass oCls : mapOrderDmQty.get(dm).values()) {
                    String PrdName = mapPrdInfo.get(oCls.OrderPrdId).PrdName;
                    String PrdSKU = mapPrdInfo.get(oCls.OrderPrdId).PrdSKU;
                    String PrdRecType = mapPrdInfo.get(oCls.OrderPrdId).PrdRTypeName;
                    String PrdType = mapPrdInfo.get(oCls.OrderPrdId).PrdType;
                    // Insert one product each order for report
                    listOrderQty.add(new OrderShipping__c(Hospital4Shipping__c=Hospital.Id,ShippingSheetCDT__c=CurrentDT,
                        DeliveryWays__c=dm,CreatePurpose__c='指示書R',EventPdf__c=EventId,ShippingSheetCode__c=SheetPrintCode,
                        ShippingPrd1Text__c=PrdName,OrderPrd1Qty__c=oCls.OrderQty,ShippingPrd1SKU__c=PrdSKU,IsLatest__c=true,
                        ShippingPrd1RecordType__c=PrdRecType,ShippingPrd1Type__c=PrdType));
                    // 
                    map<String, map<Integer, OrderShipping__c>> mapPrdTypeOrder = mapDmPrdTypeOrder.get(dm);
                    map<Integer, OrderShipping__c> mapOrderRow = mapPrdTypeOrder.get(PrdRecType);
                    OrderShipping__c os = mapOrderRow.get(mapOrderRow.size());
                    if(os.Hospital4Shipping__c == null) {
                        os.Hospital4Shipping__c=Hospital.Id; os.ShippingSheetCDT__c=CurrentDT; os.IsLatest__c=true;
                        os.ShippingPrd1RecordType__c=PrdRecType; os.EventPdf__c=EventId;
                        os.DeliveryWays__c=dm; os.CreatePurpose__c='指示書'; os.ShippingSheetCode__c=SheetPrintCode;
                    }
                    if(String.isBlank(os.ShippingPrd1Text__c)) {
                        os.ShippingPrd1Text__c=PrdName; os.OrderPrd1Qty__c=oCls.OrderQty;
                        os.ShippingPrd1SKU__c=PrdSKU; os.ShippingPrd1Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd2Text__c)) {
                        os.ShippingPrd2Text__c=PrdName; os.OrderPrd2Qty__c=oCls.OrderQty;
                        os.ShippingPrd2SKU__c=PrdSKU; os.ShippingPrd2Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd3Text__c)) {
                        os.ShippingPrd3Text__c=PrdName; os.OrderPrd3Qty__c=oCls.OrderQty;
                        os.ShippingPrd3SKU__c=PrdSKU; os.ShippingPrd3Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd4Text__c)) {
                        os.ShippingPrd4Text__c=PrdName; os.OrderPrd4Qty__c=oCls.OrderQty;
                        os.ShippingPrd4SKU__c=PrdSKU; os.ShippingPrd4Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd5Text__c)) {
                        os.ShippingPrd5Text__c=PrdName; os.OrderPrd5Qty__c=oCls.OrderQty;
                        os.ShippingPrd5SKU__c=PrdSKU; os.ShippingPrd5Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd6Text__c)) {
                        os.ShippingPrd6Text__c=PrdName; os.OrderPrd6Qty__c=oCls.OrderQty;
                        os.ShippingPrd6SKU__c=PrdSKU; os.ShippingPrd6Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd7Text__c)) {
                        os.ShippingPrd7Text__c=PrdName; os.OrderPrd7Qty__c=oCls.OrderQty;
                        os.ShippingPrd7SKU__c=PrdSKU; os.ShippingPrd7Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd8Text__c)) {
                        os.ShippingPrd8Text__c=PrdName; os.OrderPrd8Qty__c=oCls.OrderQty;
                        os.ShippingPrd8SKU__c=PrdSKU; os.ShippingPrd8Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd9Text__c)) {
                        os.ShippingPrd9Text__c=PrdName; os.OrderPrd9Qty__c=oCls.OrderQty;
                        os.ShippingPrd9SKU__c=PrdSKU; os.ShippingPrd9Type__c=PrdType;
                    } else if(String.isBlank(os.ShippingPrd10Text__c)) {
                        os.ShippingPrd10Text__c = PrdName; os.OrderPrd10Qty__c = oCls.OrderQty;
                        os.ShippingPrd10SKU__c = PrdSKU; os.ShippingPrd10Type__c = PrdType;
                        // Add one more row
                        mapOrderRow.put(mapOrderRow.size() + 1, new OrderShipping__c(Hospital4Shipping__c=Hospital.Id,ShippingSheetCDT__c=CurrentDT,
                            DeliveryWays__c=dm,CreatePurpose__c='指示書',EventPdf__c=EventId,ShippingSheetCode__c=SheetPrintCode,
                            IsLatest__c=true,ShippingPrd1RecordType__c=PrdRecType));
                    }
            	} // End of Create OrderShipping By DeliveryWay
            } // End of mapOrderDmQty
            for(map<String, map<Integer, OrderShipping__c>> mapPrdTypeOrder : mapDmPrdTypeOrder.values()) {
                for(map<Integer,OrderShipping__c> mapOrderRow : mapPrdTypeOrder.values()) {
                    listOrderQty.addAll(mapOrderRow.values());
                }
            }
            for(String sId : mapStaffBessoOrder.keySet()) {
                for(String pId : mapStaffBessoOrder.get(sId).keySet()) {
                    OrderClass oCls = mapStaffBessoOrder.get(sId).get(pId);
                    listOrderQty.add(new OrderShipping__c(Hospital4Shipping__c=Hospital.Id,ShippingSheetCDT__c=CurrentDT,
                            DeliveryWays__c='別送',CreatePurpose__c='納品書',Staff4Shipping1__c=oCls.StaffId,
                            ShippingPrd1Text__c=mapPrdInfo.get(oCls.OrderPrdId).PrdName,OrderPrd1Qty__c=oCls.OrderQty,
                            ShippingPrd1SKU__c=mapPrdInfo.get(oCls.OrderPrdId).PrdSKU,IsLatest__c=true,
                            ShippingPrd1RecordType__c=mapPrdInfo.get(oCls.OrderPrdId).PrdRTypeName,
                            ShippingPrd1Type__c=mapPrdInfo.get(oCls.OrderPrdId).PrdType,
                            EventPdf__c=EventId,ShippingSheetCode__c=SheetPrintCode));
                }
            }
            for(String sId : mapStaffHonsyaOrder.keySet()) {
                for(String pId : mapStaffHonsyaOrder.get(sId).keySet()) {
                    OrderClass oCls = mapStaffHonsyaOrder.get(sId).get(pId);
                    listOrderQty.add(new OrderShipping__c(Hospital4Shipping__c=Hospital.Id,ShippingSheetCDT__c=CurrentDT,
                            DeliveryWays__c='本社',CreatePurpose__c='納品書',Staff4Shipping1__c=oCls.StaffId,
                            ShippingPrd1Text__c=mapPrdInfo.get(oCls.OrderPrdId).PrdName,OrderPrd1Qty__c=oCls.OrderQty,
                            ShippingPrd1SKU__c=mapPrdInfo.get(oCls.OrderPrdId).PrdSKU,IsLatest__c=true,
                            ShippingPrd1RecordType__c=mapPrdInfo.get(oCls.OrderPrdId).PrdRTypeName,
                            ShippingPrd1Type__c=mapPrdInfo.get(oCls.OrderPrdId).PrdType,
                            EventPdf__c=EventId,ShippingSheetCode__c=SheetPrintCode));
                }
            }
        }
    }
    public class OrderClass {
        public Id StaffId { get; set; }
        public String StaffName { get; set; }
        public String DeliveryWay { get; set; }
        public Integer OrderQty { get; set; }
        public Id OrderPrdId { get; set; }
        public OrderClass(Id sId,String sName,Decimal qty,Id PrdId) {
            StaffId = sId; StaffName = sName; 
            OrderQty = Integer.valueOf(qty);
            OrderPrdId = PrdId; DeliveryWay = 'NA';
        }
        public OrderClass(OrderClass oc) {
            StaffId = oc.StaffId; StaffName = oc.StaffName; 
            OrderQty = oc.OrderQty; OrderPrdId = oc.OrderPrdId;
            DeliveryWay = oc.DeliveryWay;
        }
    }
    public class ProductClass {
        public String PrdRTypeName { get; set; }
        public String PrdId { get; set; }
        public String PrdName { get; set; }
        public String PrdSKU { get; set; }
        public String PrdFamily { get; set; }
        public String PrdType { get; set; }
        public Integer QtyPerPack { get; set; }
        public ProductClass(String rId,String pId,String pName,String pSku,String pFamily,String pType,Decimal pQty) {
            PrdRTypeName = (mapPrdRecordType.get(rId).DeveloperName=='ScienceDietPro')?'SD':'PD';
            PrdId = pId; PrdName = pName; PrdSKU = pSku; PrdFamily = pFamily;
            PrdType = pType; QtyPerPack = (pQty!=null)?Integer.valueOf(pQty):1;
        }
    }
}