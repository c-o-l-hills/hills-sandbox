global without sharing class MyPageEventFeedingController {
    
    private static String INITIAL_VALUE = '';
    /** ペット情報 **/
    public petInfo__c petInfo { get; set; }
    /** イベント加入者 **/
    public ProgramParticipant__c userParticipant { get; set; }
    /** 選択されたイベント**/
    public Id eventPicked {get; set;}
    /** 選択されたイベント名(業務ロジックを利用しない)**/
    public String eventNamePicked {get; set;}
    /** QTYPE */
    public String qType {get; set;}
    /** Validation Message */
    public String validationMessage {get; set;}
    /** 既存イベントのタイプ*/
    /** スタッフフィーディング */
    private Static final String EventTypeInURL_1 = 'event001';
    /** 症例収集 */
    private Static final String EventTypeInURL_2 = 'event002';
    
    /** JSON OBJECT FOR EVENT */
    public String eventListJson {get; set;}
    
    public MyPageEventFeedingController() {
        debug('画面初期表示開始。');
        petInfo = new petInfo__c();
        User u = [Select Id, AccountId, ContactId From User Where Id =: userinfo.getUserId()];
        userParticipant = new ProgramParticipant__c(
                                                    Participant__c = u.ContactId,
                                                    account__c     = u.AccountId,
                                                    userMail__c    = userInfo.getUserEmail(),
                                                    OwnerId        = userinfo.getUserId()
                                                   );
       debug('画面初期表示終了。現状ログインユーザ=' + userInfo.getUserEmail() + ' role=' + userinfo.getUserType());
        
       eventListJson = '[';
       
       // イベントを取得する
       List<Event__c> tmpEventList = [SELECT Id, Name, StartDate__c, EndDate__c FROM Event__c WHERE EventTypeInUrl__c NOT IN (:EventTypeInURL_2)];
       // 既に参加済みのイベントを取得する
       List<ProgramParticipant__c> programParticipants = [SELECT Id, Name, Event__c FROM ProgramParticipant__c WHERE userMail__c =: userInfo.getUserEmail()];
       // FEEDING済みイベントを取得する
       List<PetInfo__c> petInfos = [SELECT Id, Name, EventParticipant__c FROM PetInfo__c WHERE EventParticipant__c IN : programParticipants];
       
       debug('size=' + petInfos.size());
       
       for(Event__c event : tmpEventList) {
           eventListJson += createEventObject(event, programParticipants, petInfos);
           eventListJson += ',';
       }
       eventListJson += ']';
    }
    
    private String createEventObject(Event__c event, List<ProgramParticipant__c> programParticipants, List<PetInfo__c> petInfos) {
        String eventListJson = '{';
        eventListJson += '"value": "' + event.Id    + '",';
        
        String optionLabel = event.Name;
        for(ProgramParticipant__c programParticipant : programParticipants) {
            // イベントが申請済みの場合
            if(programParticipant.Event__c == event.Id) {
                
                for(PetInfo__c petInfo : petInfos) {
                    if(petInfo.EventParticipant__c == programParticipant.Id) {
                        // 既にFEEDING済み
                        eventListJson += '"disabled" : true,';
                        eventListJson += '"text" : "' + event.Name + ' - フィーディング済み",';
                        eventListJson += '}';
                        
                        debug('フィーディング済みイベントを見つかりました-->' + petInfo.Name + '|' + event.Name + '|' + event.Id);
                        return eventListJson;
                    }
                }
                if(event.EndDate__c < Date.today()) {
                    // FEEDING待ち状態期間切れ
                    eventListJson += '"disabled" : true,';
                    eventListJson += '"text" : "' + event.Name + ' - 時間切れ",';
                    eventListJson += '}';
                    return eventListJson;
                } else {
                    // FEEDING待ち状態
                    eventListJson += '"disabled" : false,';
                    eventListJson += '"text" : "' + event.Name + ' - フィーディング待ち",';
                    eventListJson += '}';
                    return eventListJson;
                }
            }
        }
        // イベント可能かを確認する
        if(event.StartDate__c > Date.today()) {
            // 1.開始時間は今日後（今日含め）
            eventListJson += '"disabled" : true,';
            eventListJson += '"text" : "' + event.Name + ' - イベント未開始",';
            eventListJson += '}';
            return eventListJson;
        } else if(event.EndDate__c < Date.today()) {
            // 2.終了時間は今日前
            eventListJson += '"disabled" : true,';
            eventListJson += '"text" : "' + event.Name + ' - イベント終了",';
            eventListJson += '}';
            return eventListJson;
        }

        eventListJson += '"disabled" : false,';
        eventListJson += '"text" : "' + event.Name + '",';
        eventListJson += '}';
        return eventListJson;
    }
    
    /** ユーザ認証確認 **/
    public PageReference forwardToCustomAuthPage() {
        debug('ログイン済みユーザ：' + UserInfo.getUserType() + '?' + UserInfo.getUserEmail());
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Mylogin;
        }
        return null;
    }

    public PageReference resetPageQues() {
        if(String.isNotBlank(petInfo.ProductIsUsingNow__c)){
            for(String k : product2QuesMap.keySet()){
                if(petInfo.ProductIsUsingNow__c.indexOf(k) != -1){
                    qType = product2QuesMap.get(k);
                    return null;
                }
            }
            return null;
        }
        return null;
    }
    
    /**
     * 入力データチェック
     */
    public DataValidation validate() {
        DataValidation dataValidation = new DataValidation();
        
        // 必須チェック
        dataValidation.required(1, -1, '.*', getFieldLabel('programParticipant__c', 'Event__c'),    eventPicked);                    // イベント
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'nameOfPet__c'),           petInfo.nameOfPet__c);           // ペットのお名前
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'ageOfPet__c'),            petInfo.ageOfPet__c);            // ペットの年齢
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'species__c'),             petInfo.species__c);             // 犬または猫
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'DetailClassify__c'),      petInfo.DetailClassify__c);      // 品種
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'MakerNameBeforeUsed__c'), petInfo.MakerNameBeforeUsed__c); // 今まで使用していたフードメーカー名
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'productUsedUntilNow__c'), petInfo.productUsedUntilNow__c); // 今まで使用していた製品
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'DietType__c'),            petInfo.DietType__c);            // 製品タイプ
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'ProductTyoe__c'),         petInfo.ProductTyoe__c);         // 製品タイプ
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'ProductIsUsingNow__c'),   petInfo.ProductIsUsingNow__c);   // 製品名
        
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'DoWantToRecommendToOtherOwner__c'), petInfo.DoWantToRecommendToOtherOwner__c); // 他の飼い主様に勧めてみたいと思うか
        dataValidation.required(getFieldLabel('petInfo__c', 'OfferStartDate__c'),                             petInfo.OfferStartDate__c);                // 給与開始日
        dataValidation.required(getFieldLabel('petInfo__c', 'OfferFinishDate__c'),                            petInfo.OfferFinishDate__c);               // 給与開始後
        dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'Palatability__c'),                  petInfo.Palatability__c);                  // 嗜好性
        
        // qTypeが設定された値に従って、チェック項目は変更です
        if (qType=='体重管理') {
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'ConfirmProgressOfWeightManagement__c'), petInfo.ConfirmProgressOfWeightManagement__c); // 体重管理の進捗状況
            dataValidation.required(0,           getFieldLabel('petInfo__c', 'bodyWeightBefore__c'),                  petInfo.bodyWeightBefore__c);                  // 体重（給与前）/kg
            dataValidation.required(0,           getFieldLabel('petInfo__c', 'bodyWeightAfter__c'),                   petInfo.bodyWeightAfter__c);                   // 体重（給与後）/kg
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'InspectionItem__c'),                    petInfo.InspectionItem__c);                    // 検査項目（給与前）
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'InspectionItemAfter__c'),               petInfo.InspectionItemAfter__c);               // 検査項目（給与後）  
        } else if(qType=='皮膚疾患') {
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'ConfirmProgressOfSkinManagement__c'), petInfo.ConfirmProgressOfSkinManagement__c); // 皮膚疾患の管理状況
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'skinScoreBefore__c'),                 petInfo.skinScoreBefore__c);                 // 皮膚や毛並みの状態(給与前)
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'skinScoreAfter__c'),                  petInfo.skinScoreAfter__c);                  // 皮膚や毛並みの状態(給与後)
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'PruriginemScoreBefore__c'),           petInfo.PruriginemScoreBefore__c);           // 痒みスコア(給与前)
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'PruriginemScoreAfter__c'),            petInfo.PruriginemScoreAfter__c);            // 痒みスコア(給与後)
        } else if(qType=='その他') {
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'confirmOtherProgress__c'),            petInfo.confirmOtherProgress__c); // 食事管理の進捗状況
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'InspectionItem__c'),                  petInfo.InspectionItem__c);       // 検査項目（給与前）
            dataValidation.required(1, -1, '.*', getFieldLabel('petInfo__c', 'InspectionItemAfter__c'),             petInfo.InspectionItemAfter__c);  // 検査項目（給与後）
        }
        
        return dataValidation;
        
    }

    public PageReference insertUpdateData() {
        DataValidation dataValidation = validate();
        validationMessage = '';
        if(dataValidation.hasError()) {
            validationMessage = '[';
            for(ApexPages.message message : dataValidation.messages) {
                ApexPages.addMessage(message);
                validationMessage += '\\\'' + message.getDetail() + '\\\',';
            }
            validationMessage += ']';
            debug('入力データ不正です。' + validationMessage);
            return null;
        }
                
        
        Savepoint sp = Database.setSavepoint();
        try {
            // 加入者レコードを更新する
            List<Event__c> eventList = [SELECT Id FROM Event__c WHERE Id =: eventPicked];
            if(eventList.size() > 0) {
                userParticipant.Event__c = eventPicked;
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, '選択されたイベントが既に削除されました。'));
                return null;
            }
            
            upsert userParticipant;
            debug('イベント加入者レコードが更新されました。');

            // ペット情報を新規作成
            petInfo.EventParticipant__c             = userParticipant.Id;
            petInfo.isConfirmationToServiceTerms__c = true;
            petInfo.RecordTypeId                    = CommUtils.getRecordTypeId('StaffFeeding', 'petInfo__c');
            petInfo.staff__c                        = userParticipant.Participant__c;
            petInfo.hospital__c                     = userParticipant.account__c;          
            insert petInfo;
            
            debug('ペット情報が作成されました。');
  
        } catch(Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.PETINFO_CREATE_ERR_MSG));
            System.debug(ex.getMessage() + ',' + ex.getStackTraceString());
        }

        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'ご入力いただきありがとうございました。\n今後とも日本ヒルズをよろしくお願いいたします。'));
        debug('処理終了');
        return null;
    }
    
    public Map<String, String> product2QuesMap {
        get{
            if(product2QuesMap==null){
                Map<String,String> m = new Map<String,String>();
                // 体重管理
                m.put('メタボリックス','体重管理');
                m.put('プラス','体重管理');
                m.put('モビリティ','体重管理');
                m.put('プラス','体重管理');
                m.put('ユリナリー','体重管理');
                m.put('r/d','体重管理');
                m.put('w/d','体重管理');
                // 皮膚管理
                m.put('d/d','皮膚疾患');
                m.put('z/d','皮膚疾患');
                m.put('犬ダーム ディフェンス','皮膚疾患');
                // その他
                m.put('猫c/d マルチケア','その他');
                // m.put('ダーム ディフェンス','その他');
                m.put('犬c/d マルチケア','その他');
                m.put('k/d','その他');
                m.put('i/d','その他');
                m.put('i/dローファット','その他');
                
                m.put('h/d','その他');
                m.put('j/d','その他');
                m.put('u/d','その他');
                m.put('n/d','その他');
                m.put('l/d','その他');
                m.put('a/d','その他');
                m.put('m/d','その他');
                m.put('t/d','その他');
                m.put('y/d','その他');
                m.put('健康','その他');
                return m;
            }
            return product2QuesMap;
        }
    }
    
    private String getFieldLabel(String objType, String fieldName) {
        return Schema.getGlobalDescribe().get(objType).getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel();
    }
    
    private void debug(String message) {
        System.debug(message);
    }
}