/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class MyPageCaseCollectionControllerTest {
@IsTest
    public static void testMyPageCaseCollectionController() {
       	
       	Account acc = CommUtils.createHospital();
        Contact ctc = CommUtils.createStaff(acc);
        Event__c ev = CommUtils.createEvent();
        list<ProductMaster__c> listPrd = CommUtils.createProducts();
        list<ApexSwitch__c> listApexSwitch = CommUtils.insertApexTriggerSwitch();
        ProgramParticipant__c pp = CommUtils.createEventParticipant(ev,ctc,listPrd[0]);
		
        MyPageCaseCollectionController controller = new MyPageCaseCollectionController();
        List<SelectOption> options = controller.eventOptios;
        List<SelectOption> diaOption = controller.DiagnosticOption;
        List<SelectOption> cdOption = controller.cdOption;
        
        Test.startTest();
        controller.goToMyPage();
        controller.resetPageQues();
        controller.resetCaseCollectionPageQues();
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        controller.upsertData();
        
        petInfo__c newPetInfo = new petInfo__c();
        controller.ConcurrentDisease = '併発疾患';
        controller.newPetInfo.ProductIsUsingNow__c = '体重過剰';
        controller.newPetInfo.ProductIsUsingNow_CaseCollection__c = '体重過剰';
        controller.newPetInfo.DiagnosticName__c = '体重過剰';
        controller.DiagnosticName = '体重過剰';
        controller.qType = 'qType';
        controller.diagnosType = 'diagnosType';
        controller.cdType = 'cdType';
        controller.eType = 'etpye';
        controller.goToMyPage();
        controller.resetPageQues();
        controller.resetCaseCollectionPageQues();
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        
        controller.newPetInfo.DiagnosticName__c = '大腸性下痢';
        controller.newPetInfo.ConcurrentDisease__c = '大腸性下痢';
        controller.DiagnosticName = '大腸性下痢';
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        
        controller.newPetInfo.DiagnosticName__c = '尿石症';
        controller.newPetInfo.ConcurrentDisease__c = '尿石症';
        controller.DiagnosticName = '尿石症';
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        
        controller.newPetInfo.DiagnosticName__c = '高脂血症';
        controller.newPetInfo.ConcurrentDisease__c = '高脂血症';
        controller.DiagnosticName = '高脂血症';
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        
        controller.newPetInfo.DiagnosticName__c = '糖尿病';
        controller.newPetInfo.ConcurrentDisease__c = '糖尿病';
        controller.DiagnosticName = '糖尿病';
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        
        controller.newPetInfo.DiagnosticName__c = 'ALP高値による肝・胆道疾患';
        controller.newPetInfo.ConcurrentDisease__c = 'ALP高値による肝・胆道疾患';
        controller.DiagnosticName = 'ALP高値による肝・胆道疾患';
        controller.resetCdOption();
        controller.resetDiagnosticOption();
        controller.resetDiagnosQues();
        
        controller.newPetInfo.DiagnosticName__c = 'その他（自由記入）';
        controller.DiagnosticName = 'その他（自由記入）';
        controller.resetCdOption();
        controller.resetDiagnosQues();
        
        MyPageCaseCollectionController.doSubmit(null, null, null, null, null, null);
        
        newPetInfo.isConfirmationToServiceTerms__c = true;
        newPetInfo.EventParticipant__c = pp.id;
        Database.insert(newPetInfo);
        
        MyPageCaseCollectionController.doSubmit(newPetInfo.Id, null, null, null, null, null);
        MyPageCaseCollectionController.doSubmit(newPetInfo.Id, 'Body', 'Bill', null, null, null);
        MyPageCaseCollectionController.doSubmit(newPetInfo.Id, 'Body', 'Bill', 'Body1', 'Rose', null);
        
        Test.stopTest();
    }
}