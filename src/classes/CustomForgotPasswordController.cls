/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class CustomForgotPasswordController {
    public String username {get; set;}
    public String email {get; set;}
    public String param {
        get{
            param = ApexPages.currentPage().getParameters().get('param');
            return param;
        }
        set;
    }

    public CustomForgotPasswordController() {}
    
    public PageReference forgotPassword() {
        if(email == '' || email == null) {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'メールアドレスをご入力ください。'));
             return null;
        }
        if(email != '' && email != NULL) {
            username = [SELECT Username FROM USER WHERE Email = :email][0].Username;
        }
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.CustomForgotPasswordConfirm;
        pr.getParameters().put('param', param);
        pr.setRedirect(true);
        
                    
        return pr;
        
       
    }
}