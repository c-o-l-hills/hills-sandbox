/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class DevMyLoginController {
    
    private Static final String EventTypeInURL_1 = 'event001';
    private Static final String EventTypeInURL_2 = 'event002';
    
    global String username { get; set; }
    global String password { get; set; }
    global String pagePara { get; set; }
    public String EventTypeInURL { get; set; }
    public String PageName { get; set; }
    
    global DevMyLoginController () {
        PageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        // パラメータが'event001'、'event002'以外の場合、パラメータエラーメッセージを表示する。
        if (EventTypeInURL != EventTypeInURL_1 &&
            EventTypeInURL != EventTypeInURL_2) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.LOGIN_PAGE_PARA_ERR_MSG));
        }
    }
    
    global PageReference chkIfStaff() {return null;}
    global PageReference forwardToCustomAuthPage() {
        return Page.Mylogin;
    }
    global PageReference login() {
        EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        if(!String.isNotBlank(username)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, '[ユーザ名] 項目に値を入力してください。'));
            return null;
        }
        if(!String.isNotBlank(password)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, '[パスワード] 項目に値を入力してください。'));
            return null;
        }
        if(String.isNotBlank(EventTypeInURL)){
            // get event info
            List<Event__c> eList = [SELECT Id FROM Event__c WHERE EventTypeInURL__c = :EventTypeInURL And IsOverdue__c = false LIMIT 1 ];
            if(eList.size() >0 ) {
                String AfterLoginPage;
                //スタッフフィーディング：
                //https://[sandbox domain]/a/event001
                //ログイン→　マイページ（スタッフフィーディング）
                if(EventTypeInURL == EventTypeInURL_1) {
                    AfterLoginPage = page.MyPage.getUrl(); 
                    //return Site.login(username, password, page.MyPage.getUrl() + ('?eType='+eType));
                } 
                //症例収集：
                //https://[sandbox domain]/a/event002
                //ログイン→　マイページ（症例収集）
                if(EventTypeInURL == EventTypeInURL_2) {
                    AfterLoginPage = Page.MyPageCaseCollection.getUrl();
                    //return Site.login(username, password, page.MyPageCaseCollection.getUrl() + ('?eType='+eType));
                }
                List<ProgramParticipant__c> ParticipantList = [Select Id, Name From ProgramParticipant__c 
                                                                Where Event__r.EventTypeInURL__c = :EventTypeInURL AND userMail__c = :username];
                if(ParticipantList.size() == 0){
                    if(String.isNotBlank(AfterLoginPage)){
                        return Site.login(username, password, AfterLoginPage + '?eType=' + eList.get(0).Id);
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Event_Participation));
                    return null;
                }
            }
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.URLERRORMESSAGE));
        return null;
    }
    global PageReference Register() {
        String EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        if(EventTypeInURL == EventTypeInURL_1) {
            PageReference p = page.Register;
            p.getParameters().put('param', EventTypeInURL_1);
            p.setRedirect(true);
            return p;
        }
        if(EventTypeInURL == EventTypeInURL_2) {
            PageReference p = page.Register;
            p.getParameters().put('param', EventTypeInURL_2);
            p.setRedirect(true);
            return p;
        }
        return null;
    }
    
    
}