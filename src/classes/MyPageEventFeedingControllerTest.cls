@isTest 
private class MyPageEventFeedingControllerTest {
    
    public static testMethod void testForGuestLogin() {       
        // ユーザは未ログインの場合 ログイン画面へ遷移
        List<User> guestUsers = [select id, name from User where userType = 'Guest'];        
        System.runAs(guestUsers.get(0)) {
            MyPageEventFeedingController controller = new MyPageEventFeedingController();
            controller.forwardToCustomAuthPage();
            System.assertNotEquals(Page.Mylogin, controller.forwardToCustomAuthPage());
        }
    }
    
    public static testMethod void testForEventOptios() {
        MyPageEventFeedingController controller = new MyPageEventFeedingController();
        System.assertNotEquals(null, controller.eventOptios);
    }
    
    public static testMethod void testForResetPageQues() {
        MyPageEventFeedingController controller = new MyPageEventFeedingController();
        // petInfo(今まで使用していた製品) 値設定がなし
        controller.resetPageQues();

        // petInfo(今まで使用していた製品) 値設定があり
        controller.petInfo.ProductIsUsingNow__c = 'メタボリックス';
        controller.resetPageQues();
    }
    
    // [一時]下記テスト、単純なCoverage網羅のため、観点は確報できません。
    // [正常系]はすべて項目設定は確報できるため、テスト不要  
    public static testMethod void testForDataInValidateQType() {
        MyPageEventFeedingController controller = new MyPageEventFeedingController();
        controller.qType = '体重管理';
        controller.validate();
        
        controller.qType = '皮膚疾患';
        controller.validate();
        
        controller.qType = 'その他';
        controller.validate();
    }
    
    // 正常系テスト「テストデータを事前準備必要」
    public static  testMethod void normalTest() {
        MyPageEventFeedingController controller = new MyPageEventFeedingController();
        controller.eventPicked = 'a00O000000aRgWmIAK';                          // イベント[test003]
        controller.petInfo.nameOfPet__c = 'テスト用ペット';                     // ペットのお名前
        controller.petInfo.ageOfPet__c = '1歳未満';                             // ペットの年齢
        controller.petInfo.species__c = '犬';                                   // 犬または猫 
        controller.petInfo.DetailClassify__c = '柴犬';                          // 品種
        controller.petInfo.MakerNameBeforeUsed__c = 'ヒルズ';                   // 今まで使用していたフードメーカー名
        controller.petInfo.productUsedUntilNow__c = 'テスト製品';               // 今まで使用していた製品
        controller.petInfo.DietType__c = 'プリスクリプション・ダイエット';      // 現在使用しているフードの種類
        controller.petInfo.ProductTyoe__c = '犬 ドライ製品';                    // 製品タイプ
        controller.petInfo.ProductIsUsingNow__c = '犬c/d マルチケア';           // 製品名
        controller.petInfo.DoWantToRecommendToOtherOwner__c = 'はい';           // 他の飼い主様に勧めてみたいと思うか
        controller.petInfo.OfferStartDate__c = Date.newInstance(2017, 03, 15);  // 給与開始日
        controller.petInfo.OfferFinishDate__c = Date.newInstance(2017, 03, 16); // 給与開始後
        controller.petInfo.Palatability__c = '1：不満';                         // 嗜好性
        
        controller.petInfo.ConfirmProgressOfWeightManagement__c= '食事管理の進捗状況Dummy値';    // 体重管理の進捗状況
        controller.petInfo.bodyWeightBefore__c= 10.0;                 // 体重（給与前）/kg
        controller.petInfo.bodyWeightAfter__c= 11.0;                  // 体重（給与後）/kg
        controller.petInfo.InspectionItem__c= '01.1>?';               // 検査項目（給与前）
        controller.petInfo.InspectionItemAfter__c= '01.2>?';          // 検査項目（給与後）
        
        controller.petInfo.ConfirmProgressOfSkinManagement__c= '';    // 皮膚疾患の管理状況
        controller.petInfo.skinScoreBefore__c= '';                    // 皮膚や毛並みの状態(給与前)
        controller.petInfo.skinScoreAfter__c = '';                    // 皮膚や毛並みの状態(給与後)
        controller.petInfo.PruriginemScoreBefore__c= '';              // 痒みスコア(給与前)）
        controller.petInfo.PruriginemScoreAfter__c= '';               // 痒みスコア(給与後)        
        
        controller.petInfo.confirmOtherProgress__c= '';               // 食事管理の進捗状況
        controller.petInfo.InspectionItem__c= '';                     // 検査項目（給与前）
        controller.petInfo.InspectionItemAfter__c= '';                // 検査項目（給与後）
        
        controller.insertUpdateData();
    }
    
}