/**
* An apex class that creates a portal user
*/
public without sharing class MyRegisterController {

    public ProgramParticipant__c userParticipant {get; set;}
    public Contact cont {get; set;}
    public String param {
        get {
            param = ApexPages.currentPage().getParameters().get('param');
            return param;
        }
        set;
    }
    
    public MyRegisterController () {
        cont = new Contact();
        newUser = new User();
        userParticipant = new ProgramParticipant__c();
        system.debug('●●●●●●userParticipant' + userParticipant);
        return;
    }
    
    public User newUser {get; set;}
    public String username {get; set;}
    //public String email {get; set;}
    public String email {get; set { email = value == null ? value : value.trim(); } }
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    //public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public String firstName {get; set;}
    public String lastName {get; set;}
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    public PageReference registerUser() {
        String message = '';
        if(cont.AccountId == null) {
            message += ' ・ 動物病院名: 値を入力してください<br/>';
        }
        if(email == '' || email == null) {
            message += ' ・ メール: 値を入力してください<br/>';
        }
        if(lastName == '' || LastName == null) {
            message += ' ・ 姓: 値を入力してください<br/>';
        }
        if(newUser.firstName == '' || newUser.firstName == null) {
            message += ' ・ 名: 値を入力してください<br/>';
        }
        if(password == '' || password == null) {
            message += ' ・ パスワード： 値を入力してください<br/>';
        }
        if(confirmPassword == '' || confirmPassword == null) {
            message += ' ・ パスワード確認： 値を入力してください<br/>';
        }
        if(message != '' && message != null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
            return null;
        }
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            String error = Label.site.passwords_dont_match;
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }
        List<User> uList = [SELECT Id, Name FROM User WHERE email = :email];
        if(uList.size() > 0) {
            String error = 'そのメールアドレスはすでに存在します。<br/>一意のメールアドレスを入力してください。';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error);
            ApexPages.addMessage(msg);
            return null;
        }
        
        //イベント情報取得
        String eType = '';
        String EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        
        String accountId = cont.AccountId;
        //String staffId = ApexPages.currentPage().getParameters().get('staff');
        //system.debug('●●●●●●' + staffId);
        //https://c.ap2.visual.force.com/apex/register?staff=a002800000sQR71
        // lastName is a required field on user, but if it isn't specified, we'll default it to the username

        //String userName = newUser.email;
        String userName = email;

        User u = new User();
        u.Username = userName;
        u.Email = email;
        u.FirstName = newUser.firstName;
        u.LastName = lastName;
        String nicknamePlus = datetime.now().format('HHmm', 'JST');
        u.CommunityNickname = lastName + nicknamePlus;
        
        String userId;

        try {
            userId = Site.createExternalUser(u, accountId, password);
        } catch(Site.ExternalUserCreateException ex) {
            List<String> errors = ex.getDisplayMessages();
            for (String error : errors)  {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
            }
            
            // This message is used for debugging. Do not display this in the UI to the end user.
            // It has the information around why the user creation failed.
            System.debug(ex.getMessage());
        }
        if (userId != null) { 
            if (password != null && password.length() > 1) {
                
                if(String.isNotBlank(EventTypeInURL)){    
                    // get event info
                    /*List<Event__c  > eList = [SELECT Id
                                          FROM
                                          Event__c  
                                          WHERE
                                          EventTypeInURL__c = :EventTypeInURL
                                          LIMIT 1
                                         ];
                
                    if(eList.size()>0){
                        eType = eList.get(0).Id;
                    
                        //スタッフフィーディング：https://[sandbox domain]/a/event001
                        if(EventTypeInURL == 'event001'){
                            return Site.login(username, password, page.MyPage.getUrl() + ('?eType='+eType));
                        }
                        //症例収集：https://[sandbox domain]/a/event002
                        if(EventTypeInURL == 'event002'){
                            return Site.login(username, password, page.MyPageCaseCollection.getUrl() + ('?eType='+eType));
                        }
                        return Site.login(username, password, page.MyLogin.getUrl() + ('?eType='+eType));
                    }*/
                    //PageReference p = page.myLogin;
                    PageReference p = System.page.RegisterConfirm;
                    if(EventTypeInURL == 'event001') {
                        p.getParameters().put('param', 'event001');
                    }
                    if(EventTypeInURL == 'event002') {
                        p.getParameters().put('param', 'event002');
                    }
                    p.setRedirect(true);
                    return p;
                }
                
                //return Site.login(u.Username, password, page.SelectEventPage.getUrl());
                /*PageReference p = new PageReference(retURL);
                p.setRedirect(true);
                return p;*/
            }
            else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm;
                page.setRedirect(true);
                return page;
            }
        }
        return null;
        
    }
    public PageReference toLogin() {
        String EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        PageReference page = System.page.myLogin;
        if(String.isNotBlank(EventTypeInURL)) {
            if(EventTypeInURL == 'event001') {
                page.getParameters().put('param', 'event001');
            }
            if(EventTypeInURL == 'event002') {
                page.getParameters().put('param', 'event002');
            }
        }
        page.setRedirect(true);
        return page;
    }
}