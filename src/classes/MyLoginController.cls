/**
 * An apex page controller that exposes the site login functionality
 */
global without sharing class MyLoginController {
    
    private Static final String EventTypeInURL_1 = 'event001';
    private Static final String EventTypeInURL_2 = 'event002';
    
    global String username { get; set; }
    global String password { get; set; }
    global String pagePara { get; set; }
    public String EventTypeInURL { get; set; }
    public Event__c JoinEvent { get; private set; }
    public String PageName { get; set; }
    public Boolean IsError { get; set; }
    
    global MyLoginController () {
        IsError = false;
        PageName = ApexPages.currentPage().getUrl().substringBetween('apex/', '?');
        EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        if(String.isBlank(EventTypeInURL)) { 
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.LOGIN_PAGE_PARA_ERR_MSG));
            IsError = true;
        } else {
            // パラメータが'event001'、'event002'以外の場合、パラメータエラーメッセージを表示する。
            List<Event__c> eList = [SELECT Id,IsStarted__c,IsOverdue__c FROM Event__c WHERE EventTypeInURL__c = :EventTypeInURL];
            if (eList.size() == 0) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.URLERRORMESSAGE));
                IsError = true;
            } else {
                JoinEvent = eList[0];
                if(JoinEvent.IsStarted__c) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Event_has_not_started));
                    IsError = true;
                }
                if(JoinEvent.IsOverdue__c) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Event_has_expired));
                    IsError = true;
                }
            }
        }
    }
    global PageReference forwardToCustomAuthPage() {
        return Page.Mylogin;
    }
    global PageReference login() {
        IsError = false;
        if(String.isBlank(username)) { 
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.USERNAME_IS_EMPTY));
            return null;
        }
        if(String.isBlank(password)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.PASSWORD_IS_EMPTY));
            return null;
        }
        if(JoinEvent != null){
            // get event info
            List<ProgramParticipant__c> ParticipantList = [Select Id, userMail__c From ProgramParticipant__c 
                                            Where Event__c =: JoinEvent.Id And userMail__c =: username];
            String AfterLoginPage = null;
            if(EventTypeInURL == EventTypeInURL_1) {
                if(ParticipantList.size() > 0) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Event_Participation));
                    return null;
                } else AfterLoginPage = page.MyPage.getUrl();
            }
            // 20170508 症例收集能重複
            if(EventTypeInURL == EventTypeInURL_2) AfterLoginPage = Page.MyPageCaseCollection.getUrl();
            
            if(String.isNotBlank(AfterLoginPage))
                return Site.login(username, password, AfterLoginPage + '?eType=' + JoinEvent.Id);
            
            /*if(eList.size() > 0 ) {
                String AfterLoginPage;
                //スタッフフィーディング：
                //https://[sandbox domain]/a/event001
                //ログイン→　マイページ（スタッフフィーディング）
                if(EventTypeInURL == EventTypeInURL_1) {
                    AfterLoginPage = page.MyPage.getUrl(); 
                    //return Site.login(username, password, page.MyPage.getUrl() + ('?eType='+eType));
                } 
                //症例収集：
                //https://[sandbox domain]/a/event002
                //ログイン→　マイページ（症例収集）
                if(EventTypeInURL == EventTypeInURL_2) {
                    AfterLoginPage = Page.MyPageCaseCollection.getUrl();
                    //return Site.login(username, password, page.MyPageCaseCollection.getUrl() + ('?eType='+eType));
                }
                List<ProgramParticipant__c> ParticipantList = [Select Id, Name From ProgramParticipant__c 
                                                                Where Event__r.EventTypeInURL__c = :EventTypeInURL AND userMail__c = :username];
                if(ParticipantList.size() == 0){
                    if(String.isNotBlank(AfterLoginPage)){
                        return Site.login(username, password, AfterLoginPage + '?eType=' + eList.get(0).Id);
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, Label.Duplicate_Event_Participation));
                    return null;
                }
            }*/
        } else // if Join Event doesn't exist
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.URLERRORMESSAGE));
        return null;
    }
    global PageReference login2() {
        // 症例収集
        return Site.login(username, password, page.MyPageCaseCollection.getUrl() + '?eType=a00O000000ZRdLt' );
    }
    global PageReference Register() {
        String EventTypeInURL = ApexPages.currentPage().getParameters().get('param');
        if(EventTypeInURL == EventTypeInURL_1) {
            PageReference p = page.Register;
            p.getParameters().put('param', EventTypeInURL_1);
            p.setRedirect(true);
            return p;
        }
        if(EventTypeInURL == EventTypeInURL_2) {
            PageReference p = page.Register;
            p.getParameters().put('param', EventTypeInURL_2);
            p.setRedirect(true);
            return p;
        }
        return null;
    }
    
    
}