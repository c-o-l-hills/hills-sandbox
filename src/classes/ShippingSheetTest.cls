@IsTest
public class ShippingSheetTest {
    @IsTest
    public static void testShippingSheet() {
    	Account hospital = CommUtils.createHospital();
    	Contact staff01 = CommUtils.createStaff(hospital);
    	Event__c ev01 = CommUtils.createEvent();
    	list<ProductMaster__c> listPrd = CommUtils.createProducts();
    	list<ApexSwitch__c> listApexSwitch = CommUtils.insertApexTriggerSwitch();
    	ProgramParticipant__c pp01 = CommUtils.createEventParticipant(ev01,staff01,listPrd[0]);
    	
    	list<OrderShipping__c> listOrder = new list<OrderShipping__c>();
    	for(Integer i = 1; i <= 36; i++) {
    		if(i>=1 && i<=9 ) {
    			listOrder.add(new OrderShipping__c(EventParticipant__c=pp01.Id,OrderPrd1__c=listPrd[2].Id,OrderPrd1Qty__c=3,
    				OrderPrd2__c=listPrd[1].Id,OrderPrd2Qty__c=2,OrderPrd3__c=listPrd[0].Id,OrderPrd3Qty__c=1));
    		} else if(i>9 && i<=18) {
    			listOrder.add(new OrderShipping__c(EventParticipant__c=pp01.Id,OrderPrd1__c=listPrd[5].Id,OrderPrd1Qty__c=3,
    				OrderPrd2__c=listPrd[4].Id,OrderPrd2Qty__c=2,OrderPrd3__c=listPrd[3].Id,OrderPrd3Qty__c=1));
    		} else if(i>18 && i<=27) {
    			listOrder.add(new OrderShipping__c(EventParticipant__c=pp01.Id,OrderPrd1__c=listPrd[8].Id,OrderPrd1Qty__c=3,
    				OrderPrd2__c=listPrd[7].Id,OrderPrd2Qty__c=2,OrderPrd3__c=listPrd[6].Id,OrderPrd3Qty__c=1));
    		} else if(i>27 && i<=36) {
    			listOrder.add(new OrderShipping__c(EventParticipant__c=pp01.Id,OrderPrd1__c=listPrd[11].Id,OrderPrd1Qty__c=3,
    				OrderPrd2__c=listPrd[10].Id,OrderPrd2Qty__c=2,OrderPrd3__c=listPrd[9].Id,OrderPrd3Qty__c=1));
    		}
    	} insert listOrder;
    	list<OrderPrintSetting__c> listSetting = CommUtils.insertOrderPrintSetting();
    	OrderShippingUtils.calHospitalOrder(hospital.Id); // test webservice
    	map<String, map<String, OrderShippingUtils.ShippingClass>> mapEventByHospital = 
    	    OrderShippingUtils.calcOrderShipping(new list<String> { hospital.Id });
    	OrderShippingUtils.InsertOrder(mapEventByHospital);
        system.assert(mapEventByHospital.containsKey(hospital.Id),'病院不存在.');
        system.assert(mapEventByHospital.get(hospital.Id).containsKey(ev01.Id),'Event不存在.');
        test.startTest();
        	ApexPages.StandardController sc = new ApexPages.StandardController(hospital);
            ShippingSheetCtrl ssCtrl = new ShippingSheetCtrl(sc);
        	ssCtrl.GoPrint(); 
        	// 別送納品書
        	PageReference pf = Page.ShippingSheetDetail;
            pf.getParameters().put('Id', hospital.Id);
            pf.getParameters().put('dm', '別送');
            Test.setCurrentPage(pf);
            ShippingSheetDetail ssDetail = new ShippingSheetDetail(sc);
            ssDetail.CreateLog();
    	test.stopTest();
    }
}