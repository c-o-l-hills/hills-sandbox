global without sharing class MyPageController {
    public petInfo__c  newPetInfo { get; set; }  
    public ProgramParticipant__c userParticipant { get; set; }  
    private static String INITIAL_VALUE = '';
    private boolean insertFlg = false;
    
    public List<SelectOption> eventOptios{
        get {
            if(eventOptios==null){
                
                List<SelectOption> options = new List<SelectOption>();
                
                // 選択リストの一番上にデフォルトの選択値を設定
                options.add(new SelectOption(INITIAL_VALUE, '-- なし --'));
                
                // get Event
                List<Event__c > pList = [SELECT Id,Name
                                         FROM
                                         Event__c 
                                        ];
                
                if(pList.size()!=0){
                    for( Event__c e : pList ){
                        options.add(new SelectOption(e.Id, e.Name));
                    }
                }

                return options;
            }
            system.debug('●●●●●●eventOptios' + eventOptios );
            return eventOptios;
        }
    }
    
    public Map<String,String>product2QuesMap{
        get{
            if(product2QuesMap==null){
                Map<String,String> m = new Map<String,String>();
                // 体重管理
                m.put('メタボリックス','体重管理');
                m.put('プラス','体重管理');
                m.put('モビリティ','体重管理');
                m.put('プラス','体重管理');
                m.put('ユリナリー','体重管理');
                m.put('r/d','体重管理');
                m.put('w/d','体重管理');
                // 皮膚管理
                m.put('d/d','皮膚疾患');
                m.put('z/d','皮膚疾患');
                m.put('犬ダーム ディフェンス','皮膚疾患');
                // その他
                m.put('猫c/d マルチケア','その他');
                // m.put('ダーム ディフェンス','その他');
                m.put('犬c/d マルチケア','その他');
                m.put('k/d','その他');
                m.put('i/d','その他');
                m.put('i/dローファット','その他');
                
                m.put('h/d','その他');
                m.put('j/d','その他');
                m.put('u/d','その他');
                m.put('n/d','その他');
                m.put('l/d','その他');
                m.put('a/d','その他');
                m.put('m/d','その他');
                m.put('t/d','その他');
                m.put('y/d','その他');
                m.put('健康','その他');
                return m;
            }
            return product2QuesMap;
        }
    }
    
    public String qType {get; set;} 
    public String eType {get; set;} 
    
    public PageReference resetPageQues() {
        
        if(String.isNotBlank(newPetInfo.ProductIsUsingNow__c)){
            for(String k : product2QuesMap.keySet()){
                if(newPetInfo.ProductIsUsingNow__c.indexOf(k)!=-1){
                    qType = product2QuesMap.get(k);
                }
            }
            return null;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Event.'));
        return null;
    }
    public PageReference goToMyPage() {
        
        if(String.isNotBlank(eType)){
            PageReference page = System.Page.MyPage;
            page.getParameters().put('eType', eType);
            page.setRedirect(true);
            return page;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Event.'));
        return null;
    }
    // Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        String eType = ApexPages.currentPage().getParameters().get('eType');
        if(String.isBlank(eType)) {
            pageReference page = new PageReference('/apex/mylogin');
            return page;
        }
        return null;
        /*if(UserInfo.getUserType() == 'Guest'){
            return Page.Mylogin;
            //return new PageReference('/login');
        }else{
            
            String pageName = ApexPages.CurrentPage().getUrl();
            String eType = ApexPages.currentPage().getParameters().get('eType');

        system.debug('●●●●●●eType' + pageName );
            if(String.isBlank(eType)){
                
                if(pageName.indexOf('SelectEventPage')<0){
                    PageReference page = new PageReference('/apex/SelectEventPage');
                    return page;
                }
            }
            return null;
        }*/
    }
    public MyPageController() {
        doInit();
    }
    public void doInit() {
        newPetInfo = new petInfo__c();
        userParticipant = new ProgramParticipant__c();
        String eType = ApexPages.currentPage().getParameters().get('eType');
        
        system.debug('●●●●●●userParticipant' + eType);
        // get staff info
        List<ProgramParticipant__c > pList = [SELECT Id,
                                              staffId__c ,
                                              EventType__c ,
                                              Event__c,
                                              userMail__c,
                                              Participant__c,
                                              account__c 
                                              FROM
                                              ProgramParticipant__c 
                                              WHERE
                                              Event__c = :eType
                                              AND OwnerId =:userinfo.getUserId()
                                             ];
        
        if(pList.size()==0){
            insertFlg = true;
            // イベント参加者 新規
            User u = [Select Id,AccountId,ContactId From User Where Id=: userinfo.getUserId()];
            userParticipant = new ProgramParticipant__c(Event__c = eType, 
                                                        Participant__c = u.ContactId,
                                                        EventType__c = 'type1', 
                                                        account__c = u.AccountId,
                                                        userMail__c = userInfo.getUserEmail(),
                                                        OwnerId = userinfo.getUserId()
                                                       );
        }else{
            userParticipant = pList.get(0);
        }
        
        system.debug('●●●●●●userParticipant' + userParticipant);
        
    }
    
    global pagereference upsertData(){
        
        Savepoint sp = Database.setSavepoint();
        
        try {
            // イベント参加者 新規の場合
            if (insertFlg) {
                userParticipant.Id = null;
            }
            upsert userParticipant;
            newPetInfo.EventParticipant__c = userParticipant.Id;
            newPetInfo.isConfirmationToServiceTerms__c = true;
            newPetInfo.RecordTypeId = CommUtils.getRecordTypeId('StaffFeeding', 'petInfo__c');
            newPetInfo.staff__c = userParticipant.Participant__c;
            newPetInfo.hospital__c = userParticipant.account__c;

            insert newPetInfo;
            system.debug('●●●●●●upsertData' );
            
        } catch(Exception ex) {
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.PETINFO_CREATE_ERR_MSG));
            System.debug(ex.getMessage() + ',' + ex.getStackTraceString());
        }

        return null;
    }
    @RemoteAction
    global static String doSubmit(String newPetInfoid, String attachmentBody, String attachmentName, 
                                                       String attachmentBody1, String attachmentName1, String attachmentId) {
        
        system.debug('●●●●●●doSubmit' + attachmentBody + attachmentBody1 + attachmentName + attachmentName1);

        if(newPetInfoid != null) {
            // 給与前
            if(attachmentBody != null) {
                Attachment att = getAttachment(attachmentId);
                String newBody = '';
                if(att.Body != null) {
                    newBody = EncodingUtil.base64Encode(att.Body);
                }
                newBody += attachmentBody;
                att.Body = EncodingUtil.base64Decode(newBody);
                if(attachmentId == null) {
                    att.Name = attachmentName;
                    att.parentId = newPetInfoid;
                }
                upsert att;
            } else {
                return '給与前Attachment Body was null';
            }
            // 給与後
            if(attachmentBody1 != null) {
                Attachment att = getAttachment(attachmentId);
                String newBody = '';
                if(att.Body != null) {
                    newBody = EncodingUtil.base64Encode(att.Body);
                }
                newBody += attachmentBody1;
                att.Body = EncodingUtil.base64Decode(newBody);
                if(attachmentId == null) {
                    att.Name = attachmentName1;
                    att.parentId = newPetInfoid;
                }
                upsert att;
            } else {
                return '給与後 Attachment Body was null';
            }
        } else {
            return 'ペット情報 Id がnull';
        }
        return 'アップロード完了です。';
    }  
    
    
    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments = [SELECT Id, Body
                                        FROM Attachment 
                                        WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }
        /**
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = newPetInfo.Id;
        attachment.IsPrivate = true;
        
        try {
            insert attachment;
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
            return null;
        } finally {
            attachment = new Attachment(); 
        }
        
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
        return null;
        
        return null;

    public Attachment attachment {
        get {
            if (attachment == null)
                attachment = new Attachment();
            return attachment;
        }
        set;
    }
    
    public List<SelectOption> level2Items{
        get{
            List<SelectOption> options = new List<SelectOption>();
            
            // 選択リストの一番上にデフォルトの選択値を設定
            options.add(new SelectOption(INITIAL_VALUE, '-- なし --'));
            
        system.debug('●●●●●●newPetInfo.species__c' + newPetInfo.species__c );
            // 制御項目ctrlPicklistが選択されている時に連動項目depPicklistで選べる選択肢を取得する。
            if( newPetInfo.species__c != null || newPetInfo.species__c != INITIAL_VALUE ){
                List<DepPickListCtrl.TPicklistEntry> tPicklistEntries = 
                    DepPickListCtrl.GetDependentOptions('petInfo__c','species__c','DetailClassify__c').get(newPetInfo.species__c);
                
                for( DepPickListCtrl.TPicklistEntry e : tPicklistEntries ){
                    options.add(new SelectOption(e.value, e.label));
                }
            }
        system.debug('●●●●●●options' + options );
            return options;
        }
    }
*/
    
}