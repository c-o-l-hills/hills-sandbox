// 共通クラス
global without sharing class CommUtils {
    /**
     * レコードタイプIDを取得
     * @param recodName レコード名
     * @param objectName オブジェクト名
     * @return String
    */
    public static String getRecordTypeId(String recodName, String objectName) {
        String recordTypeId = '';
        List<RecordType> recordTypeList =
            [select Id from RecordType where DEVELOPERNAME = :recodName AND SobjectType = :objectName];

        for (RecordType recordType : recordTypeList){
            recordTypeId = recordType.Id;
            break;
        }
        return recordTypeId;
    }
    /**
     * 根据公式计算口粮数量
     * @param petInfo__c newPetInfo 
     * @param 
     * @return petInfo__c newPetInfo
    */
    public static String calcPetFood(ProgramParticipant__c pp) {
        /* Get ProductFamily name by selected product */
        map<Integer, String> mapPrdCount = new map<Integer, String>();
        if(pp.ProductRegister__c != null ) mapPrdCount.put(1, pp.ProductRegister__c);
        if(pp.ProductRegister2__c != null ) mapPrdCount.put(2, pp.ProductRegister2__c);
        map<String, ProductMaster__c> mapPrdFamily = new map<String, ProductMaster__c>();
        for(ProductMaster__c prd : [Select Id,ProductFamily__c,ProductType__c From ProductMaster__c Where Id=:mapPrdCount.values()]) {
            mapPrdFamily.put(prd.Id,prd);
        } system.debug('[Prd Count]:'+mapPrdCount+'[Prd Family]:'+mapPrdFamily);
        /* Calculate Pet weight by the ratio of prd1 and prd2 */
        Decimal ratio1, ratio2 = -1;
        if(pp.ProductRegister2__c != null && pp.Prd1_Ratio__c == null){ //如果有制品2，且没有百分比--设置为各百分之50
                ratio1 = 0.5; ratio2 = 0.5;
        }
        if(pp.ProductRegister2__c == null && pp.Prd1_Ratio__c == null){//如果没有制品2，也没有百分比
                    ratio1 = 1;
        }
        if(pp.ProductRegister2__c != null && pp.Prd1_Ratio__c != null){//如果有制品2，有百分比。
            ratio1 = pp.Prd1_Ratio__c/100; ratio2 = (1-ratio1);
        }
		if(pp.ProductRegister2__c == null && pp.Prd1_Ratio__c != null){//如果没有制品2，有百分比。
		ratio1 = pp.Prd1_Ratio__c/100;
		} system.debug('[Ratio1]:'+ratio1+'[Ratio2]:'+ratio2);
		//Decimal TotalCalorie = 0;
		//if(pp.PetType__c == 'Dog') TotalCalorie = 1.4*70*Math.sqrt(Math.sqrt(pp.PetWeight__c*pp.PetWeight__c*pp.PetWeight__c));
		//else if(pp.PetType__c =='Cat') TotalCalorie = 1.0*70*Math.sqrt(Math.sqrt(pp.PetWeight__c*pp.PetWeight__c*pp.PetWeight__c));
		/*===========*/
		list<OrderShipping__c> listOrder = new list<OrderShipping__c>();
        for(Integer key : mapPrdCount.keySet()) {
        		OrderShipping__c newOrder = new OrderShipping__c(EventParticipant__c=pp.Id,CreatedByTrigger__c=true);
        		/* Fetch products by selected product family */
        		map<String, list<ProductMaster__c>> mapPrdMaster = new map<String, list<ProductMaster__c>>();
        		list<ProductMaster__c> listPrdMaster = [Select Id,SKU__c,Weight__c,Calorie__c From ProductMaster__c 
                Where ProductFamily__c=:mapPrdFamily.get(mapPrdCount.get(key)).ProductFamily__c 
                And ProductType__c=:mapPrdFamily.get(mapPrdCount.get(key)).ProductType__c Order By Weight__c DESC];
        		newOrder.Debug_Log__c = 'Product Info: '+listPrdMaster.size()+'-->'+listPrdMaster+'\n';
        		/* Get Calorie and Start Calculation */
        		Decimal TotalCalorie = 0;
        		/* PetWeight * Product Ratio */
        		Decimal PetWeight = pp.PetWeight__c*pp.PetWeight__c*pp.PetWeight__c;
        		if(key==1) PetWeight *= ratio1; else if(key==2) PetWeight *= ratio2;
        		if(pp.PetType__c == 'Dog') TotalCalorie = 1.4*70*Math.sqrt(Math.sqrt(PetWeight));
						else if(pp.PetType__c =='Cat') TotalCalorie = 1.0*70*Math.sqrt(Math.sqrt(PetWeight));
            Decimal prdCalorie = listPrdMaster[0].Calorie__c;
            Decimal calFoodWeight = (TotalCalorie/prdCalorie)*100*30;
            newOrder.Debug_Log__c += 'calFoodWeight='+calFoodWeight+'\n';
            //30天需要食物重量
            Decimal monthFoodWeight = (calFoodWeight/1000).setScale(2,RoundingMode.HALF_UP); 
            //if(key == 1) newOrder.monthFood1__c = monthFoodWeight * ratio1;
           	//else if(key == 2) newOrder.monthFood1__c = monthFoodWeight * ratio2;
           	newOrder.monthFood1__c = monthFoodWeight;
						newOrder.Debug_Log__c += 'Monthly Food Weight:'+monthFoodWeight+'\n'; 
            /* Calculating */
		        Integer largeProducts,mediumProducts,smallProducts;
		        Decimal largeMod,mediumMod,smallMod;
		        /*從最大包的開始計算*/
		        largeProducts = Integer.valueOf(Math.floor(monthFoodWeight/listPrdMaster[0].Weight__c)); //获取商
		        largeMod = monthFoodWeight - largeProducts * listPrdMaster[0].Weight__c; 
		        if(listPrdMaster.size() == 1 && largeMod > 0) largeProducts += 1; //只有一种sku 的case且有餘數
		        if(largeProducts > 0) { //如果數量大於零
		            newOrder.OrderPrd1__c = listPrdMaster[0].Id;
		            newOrder.OrderPrd1Qty__c = largeProducts;
		        } /*如果製品有兩種，開始算中包的*/
		        if(listPrdMaster.size() >= 2 && largeMod > 0) { //there are two cases
		            mediumProducts = Integer.valueOf(Math.floor(largeMod / listPrdMaster[1].Weight__c));
		            mediumMod = largeMod - mediumProducts * listPrdMaster[1].Weight__c ; //中号求余--
		            if(listPrdMaster.size() == 2 && mediumMod > 0) mediumProducts += 1;
		            if(mediumProducts > 0) { //如果數量大於零
		                  if(newOrder.OrderPrd1__c == null) { //如果沒有製品一
		                    newOrder.OrderPrd1__c = listPrdMaster[1].Id;
		                            newOrder.OrderPrd1Qty__c = mediumProducts;
		                  } else {
		                        newOrder.OrderPrd2__c = listPrdMaster[1].Id;
		                        newOrder.OrderPrd2Qty__c = mediumProducts;
		                  }
		            }
		        }
		        if(listPrdMaster.size() == 3 && mediumMod > 0) { //第三种sku
		            smallProducts  = Integer.valueOf(Math.floor(mediumMod / listPrdMaster[2].Weight__c));
		            smallMod = mediumMod - smallProducts * listPrdMaster[2].Weight__c;//余数
		            if(smallMod > 0) smallProducts += 1; 
		            if(smallProducts > 0) {
		                if(newOrder.OrderPrd1__c == null) {
		                  newOrder.OrderPrd1__c = listPrdMaster[2].Id;
		                        newOrder.OrderPrd1Qty__c = smallProducts;
		                } else if(newOrder.OrderPrd2__c == null) {
		                  newOrder.OrderPrd2__c = listPrdMaster[2].Id;
		                        newOrder.OrderPrd2Qty__c = smallProducts;
		                } else {
		                        newOrder.OrderPrd3__c = listPrdMaster[2].Id;
		                        newOrder.OrderPrd3Qty__c = smallProducts;
		                }   
		            }                                                   
		        } listOrder.add(newOrder);  
		    } if(listOrder.size() > 0) insert listOrder;  
				return '製品を注文する';
    }
    public static void RenderPDF(Id ObjId) {
        /*PageReference pdf = Page.ShippingSheet;
        pdf.getParameters().put('Id',ObjId);
        Attachment attach = new Attachment();
        attach.ParentId = ObjId;
        attach.name = 'ShippingSheet.pdf';
        attach.body = pdf.getContentAsPDF();
        insert attach;
        //CONTENTVERSION 068    USERPROFLEFEED 0D5
        FeedAttachment feedAttach = new FeedAttachment();
        feedAttach.Title = 'test';
        feedAttach.FeedEntityId = ObjId;
        feedAttach.Type = 'Content';
        insert feedAttach;*/
        //Select f.Value, f.Type, f.Title, f.RecordId, f.FeedEntityId From FeedAttachment f
    }
    public static Boolean loginValidation() {
        if(UserInfo.getProfileId() == Label.Site_Guest_Profile_Id) return false;
        return true;
    }
    public static list<SelectOption> eventSelectOptions{
        get {
            if(eventSelectOptions==null){
                List<SelectOption> options = new List<SelectOption>();
                // 選択リストの一番上にデフォルトの選択値を設定
                options.add(new SelectOption('-1', '-- なし --'));
                for( Event__c e : [SELECT Id,Name FROM Event__c] ) {
                    options.add(new SelectOption(e.Id, e.Name));
                }
                return options;
            }
            return eventSelectOptions;
        }
    }
    /* Chatter Log */
    public static FeedItem createLogOnChatter(Id recordId, String comment) {
        return new FeedItem(Body=comment,parentId=recordId);
    }
    /* Create Hospital and Contact */
    public static Account createHospital() {
        Date JanDate = Date.newInstance(Date.today().year(),1,31);
        Date FebDate = Date.newInstance(Date.today().year(),2,28);
        Date MarDate = Date.newInstance(Date.today().year(),3,31);
        Date AprDate = Date.newInstance(Date.today().year(),4,30);
        Date MayDate = Date.newInstance(Date.today().year(),5,31);
        Date JunDate = Date.newInstance(Date.today().year(),6,30);
        Date JulDate = Date.newInstance(Date.today().year(),7,31);
        Date AugDate = Date.newInstance(Date.today().year(),8,31);
        Date SepDate = Date.newInstance(Date.today().year(),9,30);
        Date OctDate = Date.newInstance(Date.today().year(),10,31);
        Date NovDate = Date.newInstance(Date.today().year(),11,30);
        Date DecDate = Date.newInstance(Date.today().year(),12,31);
        Account acc = new Account(Name = '病院１',Status__c = System.Label.STATUS_MEMBER,IsStaffFeeding__c=true,
            Hcode__c='H123456',ShipmentDestinationCode__c='463528731',DateOfArrival01__c=JanDate,DateOfArrival02__c=FebDate,
            DateOfArrival03__c=MarDate,DateOfArrival04__c=AprDate,DateOfArrival05__c=MayDate,DateOfArrival06__c=JunDate,
            DateOfArrival07__c=JulDate,DateOfArrival08__c=AugDate,DateOfArrival09__c=SepDate,DateOfArrival10__c=OctDate,
            DateOfArrival11__c=NovDate,DateOfArrival12__c=DecDate);
        insert acc; return acc;
    }
    public static Contact createStaff(Account acc) {
        Contact ctc = new Contact(LastName = 'スタッフ',Status__c='登録',AccountId = acc.Id);
        insert ctc; return ctc;
    }
    /* 製品 */
    public static list<ProductMaster__c> createProducts() {
        list<ProductMaster__c> listPrdCount = new list<ProductMaster__c>();
        listPrdCount.add(new ProductMaster__c(Name='Prd1',SKU__c='A1111',Calorie__c=111,Weight__c=1,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd1',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd1',SKU__c='A2222',Calorie__c=111,Weight__c=2,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd1',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd1',SKU__c='A3333',Calorie__c=111,Weight__c=3,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd1',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd2',SKU__c='B1111',Calorie__c=123,Weight__c=1,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd2',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd2',SKU__c='B2222',Calorie__c=123,Weight__c=2,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd2',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd2',SKU__c='B3333',Calorie__c=123,Weight__c=3,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd2',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd3',SKU__c='C1111',Calorie__c=234,Weight__c=1,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd3',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd3',SKU__c='C2222',Calorie__c=234,Weight__c=2,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd3',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd3',SKU__c='C3333',Calorie__c=234,Weight__c=3,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd3',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd4',SKU__c='D1111',Calorie__c=345,Weight__c=1,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd4',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd4',SKU__c='D2222',Calorie__c=345,Weight__c=2,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd4',PacksInBox__c=3));
        listPrdCount.add(new ProductMaster__c(Name='Prd4',SKU__c='D3333',Calorie__c=345,Weight__c=3,
            ProductType__c='Dry',Category__c='犬',ProductFamily__c='Prd4',PacksInBox__c=3));
        insert listPrdCount; return listPrdCount;
    }
    public static User createCommunityUser(Contact ctc) {
        Profile userProfile = [Select Id From Profile Where Name = 'Hills_Custom_Commuity_User'];
        User comUser = new User(firstName = 'FirstName', email = 'test@force.com',Username='test@force.com', LastName='lastname', 
            Alias='user', CommunityNickname='nickname', TimeZoneSidKey='Asia/Tokyo', LocaleSidKey='ja_JP', 
                 EmailEncodingKey='ISO-2022-JP', LanguageLocaleKey='ja', ProfileId=userProfile.Id,ContactId=ctc.Id);
        insert comUser; return comUser;
    }
    public static Event__c createEvent() {
        Event__c ev = new Event__c(Name='Event1',StartDate__c=Date.Today(),EndDate__c=Date.Today().addDays(30));
        insert ev; return ev;
    }
    public static ProgramParticipant__c createEventParticipant(Event__c ev,Contact staff,ProductMaster__c prd) {
        ProgramParticipant__c pp = new ProgramParticipant__c();
        pp.staffId__c = 'QRcodeImg';
        pp.Participant__c = staff.Id;
        pp.PetType__c = 'Dog';
        pp.PetAge__c = 2;
        pp.PetWeight__c = 15;
        pp.ProductRegister__c = prd.Id;
        pp.Event__c = ev.Id;
        insert pp; return pp;
    }
    public static list<ApexSwitch__c> insertApexTriggerSwitch() {
        //'EventParticipantTrigger  PetInfoTrigger
        list<ApexSwitch__c> listApexSwitch = new list<ApexSwitch__c> { 
            new ApexSwitch__c(Name='EventParticipantTrigger',SwitchOn__c=true), 
            new ApexSwitch__c(Name='PetInfoTrigger',SwitchOn__c=true),
            new ApexSwitch__c(Name='OrderShippingTrigger',SwitchOn__c=true)
        }; insert listApexSwitch; return listApexSwitch;
    }
    public static list<OrderPrintSetting__c> insertOrderPrintSetting() {
        list<OrderPrintSetting__c> listSetting = new list<OrderPrintSetting__c> {
            new OrderPrintSetting__c(Name='Page1',rows_a_page__c=18),
            new OrderPrintSetting__c(Name='Page Other',rows_a_page__c=30)
        }; insert listSetting; return listSetting;
    }
}