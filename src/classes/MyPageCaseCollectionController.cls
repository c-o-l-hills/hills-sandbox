public without sharing class MyPageCaseCollectionController {
    
    private static final String DIAGNOSTIC_NAME_01 = '体重過剰';                   // 診断名 ：体重過剰
    //private static final String DIAGNOSTIC_NAME_02 = '下痢';                       // 診断名 ：下痢
    private static final String DIAGNOSTIC_NAME_02 = '大腸性下痢';                       // 診断名 ：大腸性下痢
    //private static final String DIAGNOSTIC_NAME_03 = 'ストルバイト予防';              // 診断名 ：ストルバイト予防
    private static final String DIAGNOSTIC_NAME_03 = '尿石症';              // 診断名 ：尿石症
    private static final String DIAGNOSTIC_NAME_04 = '高脂血症';                   // 診断名 ：高脂血症
    private static final String DIAGNOSTIC_NAME_05 = '糖尿病';                     // 診断名 ：糖尿病  05
    //private static final String DIAGNOSTIC_NAME_06 = 'ALP高値を伴う肝・胆道疾患';     // 診断名 ：ALP高値を伴う肝・胆道疾患
    private static final String DIAGNOSTIC_NAME_06 = 'ALP高値による肝・胆道疾患';     // 診断名 ：ALP高値による肝・胆道疾患
    private static final String DIAGNOSTIC_NAME_07 = 'その他（自由記入）';                      // 診断名 ：その他（自由記入）
    private static final String DIAGNOSTIC_TYPE_01 = '01';                         // 診断名コード ：体重過剰  01
    private static final String DIAGNOSTIC_TYPE_02 = '02';                         // 診断名コード ：下痢  02
    private static final String DIAGNOSTIC_TYPE_03 = '03';                         // 診断名コード ：ストルバイト予防  03
    private static final String DIAGNOSTIC_TYPE_04 = '04';                         // 診断名コード ：高脂血症  04
    private static final String DIAGNOSTIC_TYPE_05 = '05';                         // 診断名コード ：糖尿病  05
    private static final String DIAGNOSTIC_TYPE_06 = '06';                         // 診断名コード ：ALP高値を伴う肝・胆道疾患  06
    private static final String DIAGNOSTIC_TYPE_07 = '07';                         // 診断名コード ：その他（自由記入）  07
    
    private static final String CONCURRENTDISEASE_02 = '02';                   // 診断名コード ：大腸性下痢 02
    private static final String CONCURRENTDISEASE_03 = '03';                   // 診断名コード ：尿石症 03
    private static final String CONCURRENTDISEASE_04 = '04';                   // 診断名コード ：高脂血症 04
    private static final String CONCURRENTDISEASE_05 = '05';                   // 診断名コード ：糖尿病  05
    private static final String CONCURRENTDISEASE_06 = '06';                   // 診断名コード ：ALP高値による肝・胆道疾患 06
    
    public petInfo__c  newPetInfo {
        get;
        set;
    }  
    public ProgramParticipant__c userParticipant {
        get;
        set;
    }
    // 診断名
    public String DiagnosticName {get; set;}
    public List<SelectOption> DiagnosticOption {
        get{
            if(DiagnosticOption == null) {
                DiagnosticOption = new List<SelectOption>();
                DiagnosticOption.add(new selectoption('','-- なし --'));
                DiagnosticOption.add(new selectoption('大腸性下痢','大腸性下痢'));
                DiagnosticOption.add(new selectoption('高脂血症','高脂血症'));
                DiagnosticOption.add(new selectoption('ALP高値による肝・胆道疾患','ALP高値による肝・胆道疾患'));
                DiagnosticOption.add(new selectoption('尿石症','尿石症'));
                DiagnosticOption.add(new selectoption('糖尿病','糖尿病'));
                DiagnosticOption.add(new selectoption('体重加重','体重加重'));
                DiagnosticOption.add(new selectoption('その他（自由記入）','その他（自由記入）'));
            }
            return DiagnosticOption;
        }
        set;
    }
    // 併発疾患
    public String ConcurrentDisease {get; set;}
    public List<SelectOption> cdOption {
        get{
            if(cdOption == null) {
                cdOption = new List<SelectOption>();
                cdOption = DiagnosticOption;
            }
            return cdOption;
        }
        set;
    }
    
    private static String INITIAL_VALUE = '';
    private boolean insertFlg = false;
    
    public Map<String,String>product2QuesMap{
        get{
            if(product2QuesMap==null){
                Map<String,String> m = new Map<String,String>();
                m.put('犬w/d','犬用wd');
                m.put('犬w/d小粒','犬用wd');
                m.put('犬w/d Stew 156g','犬用wd');
                
                m.put('犬メタボリックス+モビリティ','犬用メタ＋モビリティ');
                m.put('犬メタボリックス+モビリティ 小粒','犬用メタ＋モビリティ');

                m.put('犬メタボリックス','犬用猫用メタボリックス');
                m.put('犬メタボリックス 156g','犬用猫用メタボリックス');
                m.put('猫メタボリックス','犬用猫用メタボリックス');

                return m;
            }
            return product2QuesMap;
        }
    }
    
    public String qType {get; set;} 
    public String diagnosType {get; set;} 
    public String cdType {get; set;} 
    public Boolean cdType02 {get; set;} 
    public Boolean cdType03 {get; set;}
    public Boolean cdType04 {get; set;} 
    public Boolean cdType05 {get; set;} 
    public Boolean cdType06 {get; set;} 
    public String eType {get; set;}
    public String filename1 {get; set;}
    public String body1 {get; set;}
    
    // constructor
    public MyPageCaseCollectionController() {
        doInit();
    }
    public PageReference goToMyPage() {
        
        if(String.isNotBlank(eType)){
            PageReference page = System.Page.MyPageCaseCollection;
            page.getParameters().put('eType', eType);
            page.setRedirect(true);
            return page;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Event.'));
        return null;
    }

    public PageReference resetPageQues() {
        
        if(String.isNotBlank(newPetInfo.ProductIsUsingNow__c)){
            for(String k : product2QuesMap.keySet()){
                if(k.equals(newPetInfo.ProductIsUsingNow__c)){
                    qType = product2QuesMap.get(k);
                }
            }
            return null;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Event.'));
        return null;
    }
    public PageReference resetCaseCollectionPageQues() {
        
        if(String.isNotBlank(newPetInfo.ProductIsUsingNow_CaseCollection__c)){
            for(String k : product2QuesMap.keySet()){
                if(k.equals(newPetInfo.ProductIsUsingNow_CaseCollection__c)){
                    qType = product2QuesMap.get(k);
                }
            }
            return null;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Event.'));
        return null;
    }
    
    public PageReference resetCdOption() {
        //ConcurrentDisease = '';
        cdOption = new List<SelectOption>();
        if(String.isNotBlank(DiagnosticName)) {
            for(SelectOption so : DiagnosticOption) {
                if(so.getValue() == DiagnosticName) {
                    cdOption.add(new selectoption(so.getValue(), so.getLabel(), true));
                }else {
                    cdOption.add(new selectoption(so.getValue(), so.getLabel()));
                }
            }
            
            // 体重過剰
            if(DiagnosticName == DIAGNOSTIC_NAME_01){
                diagnosType = DIAGNOSTIC_TYPE_01;
                // 下痢
            }else if(DiagnosticName == DIAGNOSTIC_NAME_02){
                diagnosType = DIAGNOSTIC_TYPE_02;
                // ストルバイト予防
            }else if(DiagnosticName == DIAGNOSTIC_NAME_03){
                diagnosType = DIAGNOSTIC_TYPE_03;
                // 高脂血症
            }else if(DiagnosticName == DIAGNOSTIC_NAME_04){
                diagnosType = DIAGNOSTIC_TYPE_04;
                // 糖尿病
            }else if(DiagnosticName == DIAGNOSTIC_NAME_05){
                diagnosType = DIAGNOSTIC_TYPE_05;
                // ALP高値を伴う肝・胆道疾患
            }else if(DiagnosticName == DIAGNOSTIC_NAME_06){
                diagnosType = DIAGNOSTIC_TYPE_06;
                //その他（自由記入）
            }else if(DiagnosticName == DIAGNOSTIC_NAME_07){
                diagnosType = DIAGNOSTIC_TYPE_07;
            }
        }else {
            for(SelectOption so : DiagnosticOption) {
                cdOption.add(new selectoption(so.getValue(), so.getLabel()));
            }
        }
        return null;
    }
    public PageReference resetDiagnosticOption() {
        String ConcurrentDiseaseValue = ConcurrentDisease;
        system.debug('-----------ConcurrentDiseaseValue='+ConcurrentDiseaseValue);
        //DiagnosticOption = new List<SelectOption>();
        if(String.isNotBlank(ConcurrentDisease)) {
            /*for(SelectOption so : cdOption) {
                if(so.getValue() == ConcurrentDisease) {
                    system.debug('---------reset!');
                    DiagnosticOption.add(new selectoption(so.getValue(), so.getLabel(), true));
                }else {
                    DiagnosticOption.add(new selectoption(so.getValue(), so.getLabel()));
                }
            }*/
            //ConcurrentDisease = ConcurrentDiseaseValue;
            
            if(ConcurrentDisease.contains(DIAGNOSTIC_NAME_02)){
                //cdType = DIAGNOSTIC_TYPE_02;
                cdType02 = true;
            }
            if(ConcurrentDisease.contains(DIAGNOSTIC_NAME_03)){
                //cdType = DIAGNOSTIC_TYPE_03;
                cdType03 = true;
            }
            if(ConcurrentDisease.contains(DIAGNOSTIC_NAME_04)){
                //cdType = DIAGNOSTIC_TYPE_04;
                cdType04 = true;
            }
            if(ConcurrentDisease.contains(DIAGNOSTIC_NAME_05)){
                //cdType = DIAGNOSTIC_TYPE_05;
                cdType05 = true;
            }
            if(ConcurrentDisease.contains(DIAGNOSTIC_NAME_06)){
                //cdType = DIAGNOSTIC_TYPE_06;
                cdType06 = true;
            }
        }else {
            /*for(SelectOption so : cdOption) {
                DiagnosticOption.add(new selectoption(so.getValue(), so.getLabel()));
            }*/
            ConcurrentDisease = '';
            cdType02 = false;
            cdType03 = false;
            cdType04 = false;
            cdType05 = false;
            cdType06 = false;
        }
        ConcurrentDisease = ConcurrentDiseaseValue;
        system.debug('----------------ConcurrentDisease='+ConcurrentDisease);

        system.debug('--------------cdType02:'+cdType02);
        system.debug('--------------cdType03:'+cdType03);
        system.debug('--------------cdType04:'+cdType04);
        system.debug('--------------cdType05:'+cdType05);
        system.debug('--------------cdType06:'+cdType06);
        return null;
    }
    
    public PageReference resetDiagnosQues() {
        if(String.isNotBlank(newPetInfo.DiagnosticName__c )){
            
            // 体重過剰
            if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_01){
                diagnosType = DIAGNOSTIC_TYPE_01;
                // 下痢
            }else if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_02){
                diagnosType = DIAGNOSTIC_TYPE_02;
                // ストルバイト予防
            }else if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_03){
                diagnosType = DIAGNOSTIC_TYPE_03;
                // 高脂血症
            }else if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_04){
                diagnosType = DIAGNOSTIC_TYPE_04;
                // 糖尿病
            }else if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_05){
                diagnosType = DIAGNOSTIC_TYPE_05;
                // ALP高値を伴う肝・胆道疾患
            }else if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_06){
                diagnosType = DIAGNOSTIC_TYPE_06;
                //その他（自由記入）
            }else if(newPetInfo.DiagnosticName__c == DIAGNOSTIC_NAME_07){
                diagnosType = DIAGNOSTIC_TYPE_07;
            }
            
            return null;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Diagnos.'));
        return null;
    }
    /*
    public PageReference resetConcurrentDiseaseQues() {
        
        if(String.isNotBlank(newPetInfo.ConcurrentDisease__c )){
            
            if(newPetInfo.ConcurrentDisease__c == DIAGNOSTIC_NAME_02){
                cdType = DIAGNOSTIC_TYPE_02;
            }else if(newPetInfo.ConcurrentDisease__c == DIAGNOSTIC_NAME_03){
                cdType = DIAGNOSTIC_TYPE_03;
            }else if(newPetInfo.ConcurrentDisease__c == DIAGNOSTIC_NAME_04){
                cdType = DIAGNOSTIC_TYPE_04;
            }else if(newPetInfo.ConcurrentDisease__c == DIAGNOSTIC_NAME_05){
                cdType = DIAGNOSTIC_TYPE_05;
            }else if(newPetInfo.ConcurrentDisease__c == DIAGNOSTIC_NAME_06){
                cdType = DIAGNOSTIC_TYPE_06;
            }
            
            return null;
        }
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select the Diagnos.'));
        return null;
    }
    */
    // Code we will invoke on page load.
    /*
    public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return Page.Mylogin;
        }else{
            String pageName = ApexPages.CurrentPage().getUrl();
            String eType = ApexPages.currentPage().getParameters().get('eType');
            if(String.isBlank(eType)){                
                if(pageName.indexOf('SelectEventPage')<0){
                    PageReference page = new PageReference('/apex/SelectEventPage');
                    return page;
                }
            }
            return null;
        }
    }
    */
    
    public void doInit() {
        newPetInfo = new petInfo__c();
        List<RecordType> recordTypeList = [SELECT Id, DeveloperName, Name FROM RecordType WHERE SobjectType = 'petInfo__c' AND DeveloperName = 'CaseCollection'];
        //newPetInfo.recordTypeId = '012O00000005L93IAE';
        newPetInfo.recordTypeId = recordTypeList[0].Id;
        userParticipant = new ProgramParticipant__c();
        String eType = ApexPages.currentPage().getParameters().get('eType');
        
        // get staff info
        List<ProgramParticipant__c > pList = [SELECT Id,Participant__c,EventType__c,Event__c,userMail__c,account__c
                                              FROM ProgramParticipant__c 
                                              WHERE Event__c = :eType AND OwnerId =:userinfo.getUserId()];
        if(pList.size() == 0) {
        	insertFlg = true;
            // イベント参加者 新規
            User u = [Select Id,AccountId,ContactId From User Where Id=: userinfo.getUserId()];
            userParticipant = new ProgramParticipant__c(Event__c = eType, 
                                                        EventType__c = 'type2', 
                                                        userMail__c = userInfo.getUserEmail(),
                                                        OwnerId = userinfo.getUserId(),
                                                        Participant__c = u.ContactId,
                                                        account__c = u.AccountId
                                                       );
        }else{
            userParticipant = pList.get(0);
        }
        
    }
    //添付ファイル
    public Attachment attFile { get; set; }
    //添付ファイル名
    public String attachmentFile1name {
        get{
            attachmentFile1name = '';
            return attachmentFile1name;
        }
        set; 
    }
    //添付ファイルbody
    public Blob attachmentFile1{ 
        get{
            attachmentFile1 = null;
            return attachmentFile1;
        }
        set; 
    }
    public pagereference upsertData(){
    	
        Savepoint sp = Database.setSavepoint();
        
        try {
        	// イベント参加者 新規の場合
            if (insertFlg) {
            	userParticipant.Id = null;
            }
            upsert userParticipant;
            newPetInfo.EventParticipant__c = userParticipant.Id;
            newPetInfo.isConfirmationToServiceTerms__c = true;
            newPetInfo.RecordTypeId = CommUtils.getRecordTypeId('CaseCollection', 'petInfo__c');
            if(newPetInfo.ProductIsUsingNow_CaseCollection__c == '犬w/d小粒') {
                newPetInfo.DiagnosticName__c = DiagnosticName;
                // 取得した複数選択リストが'[a,b,c]'のため、'a;b;c'へ変換
                newPetInfo.ConcurrentDisease__c = ConcurrentDisease.removeEnd(']').removeStart('[').replace(',', ';');
            }
            insert newPetInfo;
        }catch(Exception ex){
            Database.rollback(sp);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.PETINFO_CREATE_ERR_MSG));
            System.debug(ex.getMessage() + ',' + ex.getStackTraceString());
        }
        return null;
    }
    @RemoteAction
    public static String doSubmit(String newPetInfoid, String attachmentBody, String attachmentName, 
                                                       String attachmentBody1, String attachmentName1, String attachmentId) {
            
            if(newPetInfoid != null) {
                // 給与前
                if(attachmentBody != null) {
                    Attachment att = getAttachment(attachmentId);
                    String newBody = '';
                    if(att.Body != null) {
                        newBody = EncodingUtil.base64Encode(att.Body);
                    }
                    newBody += attachmentBody;
                    att.Body = EncodingUtil.base64Decode(newBody);
                    if(attachmentId == null) {
                        att.Name = attachmentName;
                        att.parentId = newPetInfoid;
                    }
                    upsert att;
                } else {
                    return '給与前Attachment Body was null';
                }
                // 給与後
                if(attachmentBody1 != null) {
                    Attachment att = getAttachment(attachmentId);
                    String newBody = '';
                    if(att.Body != null) {
                        newBody = EncodingUtil.base64Encode(att.Body);
                    }
                    newBody += attachmentBody1;
                    att.Body = EncodingUtil.base64Decode(newBody);
                    if(attachmentId == null) {
                        att.Name = attachmentName1;
                        att.parentId = newPetInfoid;
                    }
                    upsert att;
                } else {
                    return '給与後 Attachment Body was null';
                }
            } else {
                return 'ペット情報 Id がnull';
            }
        return 'アップロード完了です。';
    }  
    
    
    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments = [SELECT Id, Body
                                        FROM Attachment 
                                        WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }
    
    
   
}