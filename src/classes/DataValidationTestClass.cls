/**
 * DataValidation用テストクラス
 */
@isTest 
private class DataValidationTestClass {
    
    // 単純なコードカバー率網羅ため
    static testMethod void testRequired() {
        DataValidation dataValidation = new  DataValidation();
        System.assertEquals(false, dataValidation.hasError());

    }
    
    // 文字列用必須チェックメソッドについてのテスト
    static testMethod void testRequiredForString() {
       DataValidation dataValidation = new  DataValidation();
       dataValidation.required(2, 10, '.*',       '正常ケース', 'abcde12345');       // 最小桁数OK　  最大桁数OK 正規表現OK -- 境界値
       dataValidation.required(2, 10, '.*',       '正常ケース', 'ab');               // 最小桁数OK　  最大桁数OK 正規表現OK -- 境界値
       dataValidation.required(2, 10, '.*',       '異常ケース', 'a');                // 最小桁数未満　最大桁数超 正規表現OK
       dataValidation.required(2, 10, '.*',       '異常ケース', 'abcde123456');      // 最小桁数OK　  最大桁数超 正規表現OK
       dataValidation.required(2, 10, '^[0-9]+$', '異常ケース', 'abcde12345');       // 最小桁数OK　  最大桁数OK 正規表現違反
       dataValidation.required(2, 10, '.*',       '異常ケース', '');                 // 最小桁数OK　  最大桁数OK 正規表現OK -- 値ブランク(最小桁数未満)
       dataValidation.required(2, 10, '.*',       '異常ケース', null);               // 最小桁数未満　最大桁数OK 正規表現NULL -- NULL違反
       dataValidation.required(-1, -1, '.*',      '正常ケース', 'abcde12345');       // 最小制限無し　最大制限無し
       
       System.assertEquals(true, dataValidation.hasError());
       System.assertEquals(5, dataValidation.messages.size());
    }
    
    // 数字(Decimal)についてのテスト
    static testMethod void testRequiredForDecimal() {
        DataValidation dataValidation = new  DataValidation();
        dataValidation.required(0, 'Decimalテスト', -1);   // 最小値より小さい
        dataValidation.required(0, 'Decimalテスト', 0);    // 境界値より同じ(正常ケース)
        dataValidation.required(0, 'Decimalテスト', 1);    // 正常ケース
        dataValidation.required(0, 'Decimalテスト', null); // 値未設定
    
        System.assertEquals(true, dataValidation.hasError());
        System.assertEquals(2, dataValidation.messages.size());
    }
    
    // 日付についてのテスト
    static testMethod void testRequiredForDate() {
        DataValidation dataValidation = new  DataValidation();
        dataValidation.required('日時チェック', Date.newInstance(1960, 2, 17));
        dataValidation.required('日時チェック', null);
        
        System.assertEquals(true, dataValidation.hasError());
        System.assertEquals(1, dataValidation.messages.size());

    }
}