public with sharing class ShippingSheetDetail {
    public Account acc { get; set; }
    public String DeliveryMethod { get { return ApexPages.currentPage().getParameters().get('dm'); } }
    public map<Id, String> mapEvent { get; set; }
    public map<Id, map<Integer, list<ProductClass>>> mapEventProduct { get; private set; }
    /* to count total pages by pre-defined row number a page. */
    public Integer totalPageCount { get; private set; }
    private Integer PAGE1_ROWS;
    private Integer PAGE_ROWS;
    
    public ShippingSheetDetail(ApexPages.StandardController stdCtrl) {
        acc = (Account)stdCtrl.getRecord();
        if(acc == null || acc.Id == null) return;
        for(OrderPrintSetting__c oSetting : [Select Name,rows_a_page__c From OrderPrintSetting__c]) {
        		if(oSetting.Name == 'Page1') PAGE1_ROWS = Integer.valueOf(oSetting.rows_a_page__c);
        		if(oSetting.Name == 'Page Other') PAGE_ROWS = Integer.valueOf(oSetting.rows_a_page__c);
        }
    }
    public void CalcDetail() {
        mapEvent = new map<Id, String>();
        mapEventProduct = new map<Id, map<Integer, list<ProductClass>>>();
        
        list<OrderShipping__c> listOrderShipping = [Select Id,Staff4Shipping1__c,Staff4Shipping1__r.Name,ShippingPrd1SKU__c,
            ShippingPrd1Text__c,ShippingPrd1RecordType__c,ShippingPrd1Type__c,OrderPrd1Qty__c,EventPdf__c,EventPdf__r.event_name_pdf__c
            From OrderShipping__c Where Hospital4Shipping__c=:acc.Id And WillShip__c=true And CreatePurpose__c='納品書' And IsLatest__c=true
            And DeliveryWays__c =: DeliveryMethod Order By ShippingPrd1Text__c,Staff__r.Name];
        totalPageCount = 1;
        for(OrderShipping__c os : listOrderShipping) {
            if(os.EventPdf__c == null) continue;
            if(!mapEvent.containsKey(os.EventPdf__c)) mapEvent.put(os.EventPdf__c,os.EventPdf__r.event_name_pdf__c);
            if(!mapEventProduct.containsKey(os.EventPdf__c))
                mapEventProduct.put(os.EventPdf__c, new map<Integer, list<ProductClass>> { totalPageCount => new list<ProductClass>() });
            map<Integer, list<ProductClass>> mapProduct = mapEventProduct.get(os.EventPdf__c);
            if(os.ShippingPrd1Text__c != null && os.OrderPrd1Qty__c > 0) {
                ProductClass cls = new ProductClass();
                if(os.Staff4Shipping1__c != null) cls.StaffName = os.Staff4Shipping1__r.Name;
                cls.PrdRType = os.ShippingPrd1RecordType__c;
                cls.PrdSKU = os.ShippingPrd1SKU__c;
                cls.PrdName = os.ShippingPrd1Text__c;
                cls.PrdType = os.ShippingPrd1Type__c;
                cls.PrdQty = Integer.valueOf(os.OrderPrd1Qty__c);
                if ((totalPageCount==1 && mapProduct.get(totalPageCount).size()<PAGE1_ROWS) || 
                    (totalPageCount<>1 && mapProduct.get(totalPageCount).size()<PAGE_ROWS)) {
                    mapProduct.get(totalPageCount).add(cls);
                } else {
                    totalPageCount = totalPageCount + 1;
                    list<ProductClass> listCls = new list<ProductClass>();
                    listCls.add(cls);
                    mapProduct.put(totalPageCount, listCls);
                }
            }
        } // End of OrderShipping looping
        for(Id eId : mapEventProduct.keySet()) {
            map<Integer, list<ProductClass>> mapProduct = mapEventProduct.get(eId);
            if (totalPageCount == 1 && mapProduct.get(1).size()<PAGE1_ROWS) {
                for (Integer i = mapProduct.get(1).size(); i < PAGE1_ROWS; i++) {
                    ProductClass cls = new ProductClass();
                    mapProduct.get(1).add(cls);
                }
            } else if (totalPageCount > 1 && mapProduct.get(totalPageCount).size()<PAGE_ROWS){
                for (Integer i = mapProduct.get(totalPageCount).size(); i < PAGE_ROWS; i++) {
                    ProductClass cls = new ProductClass();
                    mapProduct.get(totalPageCount).add(cls);
                }
            }
        }
    }
    public void CreateLog() {
        if(acc != null && acc.Id != null) {
            CalcDetail();
            if(mapEventProduct.size() > 0) {
                FeedItem feedLog = CommUtils.createLogOnChatter(acc.Id,System.Label.HonSyaShippingPrint+','+DateTime.now());
                insert feedLog;
            }
        }
    }
    public class ProductClass {
        public String StaffName { get; set; }
        public String PrdRType { get; set; }
        public String PrdName { get; set; }
        public String PrdSKU { get; set; }
        public String PrdType { get; set; }
        public Integer PrdQty { get; set; }
    }
}