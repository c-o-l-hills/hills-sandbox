/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class CommUtilsTest {
    @IsTest
    public static void testCalcPetFood() {
        /* Only RecordType = スタッフフィーディング */
        Account acc = CommUtils.createHospital();
        Contact ctc = CommUtils.createStaff(acc);
        User usr = CommUtils.createCommunityUser(ctc);
        Event__c ev = CommUtils.createEvent();
        list<ProductMaster__c> listPrd = CommUtils.createProducts();
        FeedItem feedLog = CommUtils.createLogOnChatter(ctc.Id,'Staff created.');
        CommUtils.RenderPDF(acc.Id);
        list<ApexSwitch__c> listApexSwitch = CommUtils.insertApexTriggerSwitch();
        list<OrderPrintSetting__c> listOrderPrintSetting = CommUtils.insertOrderPrintSetting();
        
        ProgramParticipant__c pp = CommUtils.createEventParticipant(ev,ctc,listPrd[0]);
        String message = CommUtils.calcPetFood(pp);
        pp.ProductRegister2__c = listPrd[3].Id;
        message = CommUtils.calcPetFood(pp);
        pp.Prd1_Ratio__c = 30;
        message = CommUtils.calcPetFood(pp);
        
        petInfo__c dogInfo = new petInfo__c(RecordTypeId=CommUtils.getRecordTypeId('StaffFeeding','petInfo__c'),
        	bodyWeightAfter__c=8,ProductIsUsingNow__c='Prd1',species__c='Dog',EventParticipant__c=pp.Id);
        
        pp.PetType__c = 'Cat';
        message = CommUtils.calcPetFood(pp);
    }
}