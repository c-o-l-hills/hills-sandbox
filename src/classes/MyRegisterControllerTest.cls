/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class MyRegisterControllerTest {
@IsTest(SeeAllData=true)
public static void testMyRegisterController() {
        
        List<Event__c> eList = new List<Event__c>();
        Event__c event = new Event__c();
        
        event.Name = 'EV1';
        event.EventTypeInURL__c = 'event001';
        eList.add(event);
        
        event = new Event__c();
        event.Name = 'EV2';
        event.EventTypeInURL__c = 'event002';
        eList.add(event);
        
        insert eList;
        
        MyRegisterController controller = new MyRegisterController();
        String param = controller.param;
        controller.param = 'event001';
        Test.startTest();
        controller.registerUser();
        controller.toLogin();
        
        Account acc = CommUtils.createHospital();
        Contact ctc = CommUtils.createStaff(acc);
        User user = new User(firstName = 'FirstName', email = 'test@force.com',Username='test@force.com', LastName='lastname', 
                                            Alias='user', CommunityNickname='nickname', TimeZoneSidKey='Asia/Tokyo', LocaleSidKey='ja_JP', 
                                            EmailEncodingKey='ISO-2022-JP', LanguageLocaleKey='ja', ProfileId='00eO0000000MNaZ', ContactId=ctc.Id);
        //insert user;
        
        controller.cont.AccountId = acc.Id;
        controller.newUser.firstName = 'FirstName';
        controller.username = 'FirstName LastName';
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd12345';
        controller.registerUser();
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd1234';
        controller.registerUser();
        
        controller.email = 'testAA@force.com';
        controller.registerUser();
        
        ApexPages.currentPage().getParameters().put('param', 'event001');
        controller.toLogin();
        
        ApexPages.currentPage().getParameters().put('param', 'event002');
        controller.toLogin();
        Test.stopTest();
     }
}