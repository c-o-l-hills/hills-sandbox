/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
@IsTest public with sharing class CustomAccountLookupControllerTest {
@IsTest(SeeAllData=true)
public static void testCustomAccountLookupController() {
        
        CustomAccountLookupController controller = new CustomAccountLookupController();
        
        Test.startTest();
        controller.searchString = '080';
        controller.search();
        
        controller.account = new Account(Name = 'rose');
        controller.saveAccount();
        
        System.assertEquals(controller.getFormTag(), null); 
        System.assertEquals(controller.getTextBox(), null);
        Test.stopTest();
     }
}