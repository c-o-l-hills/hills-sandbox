trigger OrderShippingAppend on OrderShipping__c (Before Insert, Before Update, After Insert, After Update) {
    if(ApexSwitch__c.getValues('OrderShippingTrigger').SwitchOn__c) {
        map<Id, list<OrderShipping__c>> mapEventOrder = new map<Id, list<OrderShipping__c>>();
        for(OrderShipping__c obj : trigger.new) {
            if(String.isNotBlank(obj.EventParticipant__c)) {
                if(!mapEventOrder.containsKey(obj.EventParticipant__c)) mapEventOrder.put(obj.EventParticipant__c, new list<OrderShipping__c>());
                list<OrderShipping__c> listOrder = mapEventOrder.get(obj.EventParticipant__c);
                listOrder.add(obj); mapEventOrder.put(obj.EventParticipant__c, listOrder);
            }
        }
        if(trigger.isBefore && mapEventOrder.size() > 0) {
            list<ProgramParticipant__c> listPP = [Select Id,Participant__c,account__c From ProgramParticipant__c Where Id=:mapEventOrder.keySet()];
            for(ProgramParticipant__c pp : listPP) {
                for(OrderShipping__c obj : mapEventOrder.get(pp.Id)) {
                    if(trigger.isBefore && obj.EventParticipant__c != null && obj.WillShip__c) {
                        obj.Hospital__c = pp.account__c;
                        obj.Staff__c = pp.Participant__c;
                    } 
                }
            } //if(listPP.size() > 0) update listPP;
        }
    }
}