trigger AccountAfterTrigger on Account (After Update) {
    
    if (Trigger.isUpdate) {
    	AccountTriggerHandler.processAfterUpdate(Trigger.newMap);
        
    }
}