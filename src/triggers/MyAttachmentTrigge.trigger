trigger MyAttachmentTrigge on Attachment (after update, after insert) {
    List <petInfo__c> pList = new List<petInfo__c>();
    Set<String> pIds = new Set<String>();
    Set<String> attIds = new Set<String>();
    Map<String,Set<String>> pMap = new Map<String,Set<String>>();
    for(Attachment att : trigger.New){
         //Check if added attachment is related to Account or not
         if(att.ParentId.getSobjectType() == petInfo__c.SobjectType){
             if(pMap.containsKey(att.ParentId)){
                 attIds= pMap.get(att.ParentId);
             }else{
                 attIds= new Set<String>();
             }
              attIds.add(att.Id);
              pIds.add(att.ParentId);
             pMap.put(att.ParentId,attIds);
         }
    }
    pList = [select id, attachMentId__c  from petInfo__c where id in : pIds];
    if(pList!=null && pList.size()>0){
        for(petInfo__c p : pList){
            
             if(pMap.containsKey(p.Id)){
                Set<String> attInIds = pMap.get(p.Id);
                list<string> attInIdlist = new list<string>(attInIds );
                if(attInIdlist.size()>1){
                    p.attachMentId__c = attInIdlist[0];
                    p.attachMentIdAfter__c= attInIdlist[1];
                    p.VideoIdBefore__c = attInIdlist[2];
                    p.VideoIdAfter__c = attInIdlist[3];
                }else if(attInIdlist.size()>0){        
                    if(String.isNotBlank(p.attachMentId__c)){
                        p.attachMentIdAfter__c= attInIdlist[0];
                    }else{
                        p.attachMentId__c = attInIdlist[0];
                    }
                }
                
             }
        }
        update pList;
    }
}