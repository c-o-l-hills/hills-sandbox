trigger EventParticipant on ProgramParticipant__c (before insert,before update,after insert,after update, before delete) {
if(ApexSwitch__c.getValues('EventParticipantTrigger').SwitchOn__c) {
    list<ProgramParticipant__c> listPP = (trigger.isInsert || trigger.isUpdate)?trigger.new:trigger.old;
    for(ProgramParticipant__c pp : listPP) {
    	if(trigger.isInsert || trigger.isUpdate) {
		    if(trigger.isBefore && pp.Participant__c != null) {
		        Contact ctc = [Select Id,Email,AccountId From Contact Where Id =: pp.Participant__c];
		        if(String.isNotBlank(ctc.AccountId)) pp.account__c = ctc.AccountId;
		        if(String.isNotBlank(ctc.Email)) pp.userMail__c = ctc.Email;
		    }
		    if(trigger.isAfter) {
    	        if(String.isNotBlank(pp.ProductRegister__c) && String.isNotBlank(pp.PetType__c) && Integer.valueOf(pp.PetWeight__c) >= 0) {
    		        CommUtils.calcPetFood(pp);
    	        } 
		    } // end of isAfter
        } // end of Insert or Update
        if(trigger.isDelete)
	        delete [Select Id From OrderShipping__c Where EventParticipant__c =: pp.Participant__c];
	} // end of loop of trigger.new
}
}