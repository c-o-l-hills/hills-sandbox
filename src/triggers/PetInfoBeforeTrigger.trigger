trigger PetInfoBeforeTrigger on petInfo__c (before insert) {
    
    if (Trigger.isInsert) {
        PetInfoTriggerHandler.processBeforeInsert(Trigger.new);
    }
}