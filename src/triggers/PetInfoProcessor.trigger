trigger PetInfoProcessor on petInfo__c (before insert, before update, after insert, after update) {
if(ApexSwitch__c.getValues('PetInfoTrigger').SwitchOn__c) {
    if(trigger.isBefore) {
        for(petInfo__c pet : trigger.new) {
            if(pet.EventParticipant__c != null) {
                ProgramParticipant__c pp = [Select Participant__c,account__c,account__r.Name From ProgramParticipant__c Where Id =: pet.EventParticipant__c];
                if(pp.Participant__c != null) pet.staff__c = pp.Participant__c;
                if(pp.account__c != null) pet.Hospital__c = pp.account__c;
            }
        }
    }
    if(trigger.isAfter) {
        /* In order to calculate Pet Food, the formula will need pet species, selected product and pet weight. 20170225 Cancel
        for(petInfo__c pet : trigger.new) {
            String rTypeId = CommUtils.getRecordTypeId('StaffFeeding','petInfo__c');
            if(pet.RecordTypeId == rTypeId && pet.species__c != null && pet.bodyWeightAfter__c > 0 && pet.ProductIsUsingNow__c != null) {
                String message = CommUtils.calcPetFood(pet);
            }
        }*/
    }
} // End of PetInfoTrigger
}